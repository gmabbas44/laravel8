<?php

use App\Http\Controllers\Admin\BusRegController;
use App\Http\Controllers\Admin\CoachfacilityController;
use App\Http\Controllers\Admin\CoachTypeController;
use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\DriverController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\LocationController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\TripRouteController;
use App\Http\Controllers\Admin\CounterController;
use App\Http\Controllers\Admin\TicketPriceController;
use App\Http\Controllers\Admin\TripAssignController;
use App\Http\Controllers\Admin\TripManageContoller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

// Route::get('/home', [HomeController::class, 'index'])->name('home');
// Route::get('admin/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth', 'is_admin'])->group(function () {

    Route::get('admin/home', [HomeController::class, 'adminHome'])->name('admin.route');

    // Manage Company 
    Route::get('admin/company-list', [CompanyController::class, 'index'])->name('admin.company.list');
    Route::get('admin/add-company', [CompanyController::class, 'create'])->name('admin.add.company');
    Route::post('admin/save-company', [CompanyController::class, 'save'])->name('admin.save.company');
    Route::get('admin/edit-company/{id}', [CompanyController::class, 'edit'])->name('admin.edit.company');
    Route::post('admin/update-company', [CompanyController::class, 'update'])->name('admin.update.company');
    Route::get('admin/delete-company/{id}', [CompanyController::class, 'delete'])->name('admin.delete.company');

    // Manage User
    Route::get('admin/admin-list', [UserController::class, 'index'])->name('admin.list');
    Route::get('admin/create-admin', [UserController::class, 'create'])->name('admin.add');
    Route::post('admin/save-admin', [UserController::class, 'save'])->name('admin.save');

    // Manage Coach type 
    Route::get('admin/coach-type-list', [CoachTypeController::class, 'index'])->name('admin.coach.type.list');
    Route::get('admin/add-coach-type', [CoachTypeController::class, 'create'])->name('admin.add.coach.type');
    Route::post('admin/save-coach-type', [CoachTypeController::class, 'save'])->name('admin.save.coach.type');

    // Manage coach facelity 
    Route::get('admin/coach-facility-list', [CoachfacilityController::class, 'index'])->name('admin.coach.facelity.list');
    Route::get('admin/add-coach-facility', [CoachfacilityController::class, 'create'])->name('admin.add.coach.facelity');
    Route::post('admin/save-coach-facility', [CoachfacilityController::class, 'save'])->name('admin.save.coach.facility');

    // Manage Bus registration
    Route::get('admin/bus-list', [BusRegController::class, 'index'])->name('admin.bus.list');
    Route::get('admin/bus-reg', [BusRegController::class, 'create'])->name('admin.bus.reg');
    Route::post('admin/bus-add', [BusRegController::class, 'save'])->name('admin.bus.add');
    Route::get('admin/bus-edit/{id}', [BusRegController::class, 'edit'])->name('admin.bus.edit');
    Route::post('admin/get-company-facility', [BusRegController::class, 'getCoachtypeAndfacility'])->name('admin.get.company.facility');
    Route::post('admin/bus-update', [BusRegController::class, 'update'])->name('admin.bus.update');
    Route::get('admin/bus-delete/{id}', [BusRegController::class, 'delete'])->name('admin.bus.delete');

    // Manage Drivers
    Route::get('admin/driver-list', [DriverController::class, 'index'])->name('admin.driver.list');
    Route::get('admin/driver-create', [DriverController::class, 'create'])->name('admin.driver.create');
    Route::post('admin/driver-save', [DriverController::class, 'save'])->name('admin.driver.save');
    Route::get('admin/driver-edit/{id}', [DriverController::class, 'edit'])->name('admin.driver.edit');
    Route::post('admin/driver-update', [DriverController::class, 'update'])->name('admin.driver.update');
    Route::get('admin/driver-delete/{id}', [DriverController::class, 'delete'])->name('admin.driver.delete');

    // Location list
    Route::get('admin/location-list', [LocationController::class, 'index'])->name('admin.location.list');
    Route::get('admin/location-create', [LocationController::class, 'create'])->name('admin.location.create');
    Route::post('admin/location-save', [LocationController::class, 'save'])->name('admin.location.save');
    Route::get('admin/location-edit/{id}', [LocationController::class, 'edit'])->name('admin.location.edit');
    Route::post('admin/location-update', [LocationController::class, 'update'])->name('admin.location.update');
    Route::get('admin/location-delete/{id}', [LocationController::class, 'delete'])->name('admin.location.delete');

    // Trip route manage
    Route::get('admin/route-list', [TripRouteController::class, 'index'])->name('admin.route.list');
    Route::get('admin/route-create', [TripRouteController::class, 'create'])->name('admin.route.create');
    Route::post('admin/route-save', [TripRouteController::class, 'save'])->name('admin.route.save');
    Route::get('admin/route-edit/{id}', [TripRouteController::class, 'edit'])->name('admin.route.edit');
    Route::post('admin/route-update', [TripRouteController::class, 'index'])->name('admin.route.update');
    Route::get('admin/route-delete/{id}', [TripRouteController::class, 'soft_delete'])->name('admin.route.delete');
    Route::post('admin/getCompanyRouteInfo', [TripRouteController::class, 'getCompanyRouteInfo'])->name('admin.get.company.route.info');

    // Counter manage
    Route::resource('admin/counter', CounterController::class);
    // Trip assign manage
    Route::resource('admin/trip-assign', TripAssignController::class);
    Route::post('admin/bus-info', [BusRegController::class, 'companyBusInfo'])->name('admin.companyBusInfo');
    Route::get('admin/trip-assing-edit/{id}', [TripAssignController::class, 'edit'])->name('trip.assign.edit');
    Route::post('amin/trip-assing-update', [TripAssignController:: class, 'update'])->name('trip-assign.update');
    Route::post('admin/getCompanyRoute', [TripRouteController::class, 'getCompanyRoute'])->name('admin.getCompanyRoute');

    // Ticket price 
    Route::resource('admin/ticket-price', TicketPriceController::class);
    Route::get('admin/ticket-price-edit/{id}', [TicketPriceController::class, 'edit'])->name('ticket.price.edit');
    Route::post('admin/ticket-price-update', [TicketPriceController::class, 'update'])->name('ticket-price.update');
    Route::post('getCompanyRouteCoach', [TicketPriceController::class, 'getCompanyRouteCoach'])->name('getCompanyRouteCoach');
    Route::post('getStoppage', [TripRouteController::class, 'getStoppage'])->name('getStoppage');
    Route::post('getticketprice', [TicketPriceController::class, 'getticketprice'])->name('get.ticket.price');

    // Trip search manage
    Route::get('admin/trip-search', [TripManageContoller::class, 'index'])->name('trip.search.index');
    Route::post('admin/trip-search', [TripManageContoller::class, 'gettriplist'])->name('gettriplist');
    Route::get('admin/seat-view/{id}', [TripManageContoller::class, 'seat_view'])->name('admin.seat.view');


});
