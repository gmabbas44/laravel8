<?php

namespace Database\Seeders;

use App\Models\CoachType as ModelsCoachType;
use Illuminate\Database\Seeder;

class CoachType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ModelsCoachType::factory()->create();
    }
}
