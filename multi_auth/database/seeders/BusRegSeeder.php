<?php

namespace Database\Seeders;

use App\Models\BusReg;
use Illuminate\Database\Seeder;

class BusRegSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BusReg::factory()->create();
    }
}
