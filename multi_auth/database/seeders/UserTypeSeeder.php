<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\facades\DB;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_types')->insert([
            'title' => 'Sub Super Admin',
            'description' => 'full controller',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
