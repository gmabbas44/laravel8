<?php

namespace Database\Seeders;

use App\Models\Coachfacility;
use Illuminate\Database\Seeder;

class CoachFacilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Coachfacility::factory()->create();
    }
}
