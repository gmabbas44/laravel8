<?php

namespace Database\Seeders;

use App\Models\TicketPrice;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTypeSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(CoachType::class);
        $this->call(CoachFacilitySeeder::class);
        $this->call(BusRegSeeder::class);
        $this->call(DriverSeeder::class);
        $this->call(LocationSeeder::class);
        $this->call(TripRouteSeeder::class);
        $this->call(CounterSeeder::class);
        $this->call(TicketPrice::class);
    }
}
