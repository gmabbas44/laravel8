<?php

namespace Database\Seeders;

use App\Models\TripRoute;
use Illuminate\Database\Seeder;

class TripRouteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TripRoute::factory()->create();
    }
}
