<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->id();
            $table->string('locationName');
            $table->string('locationShortName')->nullable();
            $table->string('description');
            $table->text('googleMap')->nullable();
            $table->foreignId('company_id');
            $table->tinyInteger('status')->comment('0->no, 1->yes');
            $table->tinyInteger('soft_delete')->default(0)->comment('0->no, 1->yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
