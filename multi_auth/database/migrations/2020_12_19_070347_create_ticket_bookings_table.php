<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_bookings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('counter_id')->nullable();
            $table->foreignId('tirp_route_id');
            $table->foreignId('bus_reg_id');
            $table->foreignId('tirp_assign_id');
            $table->foreignId('coach_type_id');
            $table->foreignId('passenger_id')->nullable();

            $table->string('pnr');
            $table->string('droping_point');
            $table->integer('booking_total_seat');
            $table->string('seat_numbers');
            $table->decimal('price', 8, 2);
            
            $table->decimal('total_fare_seat', 8, 2);
            $table->decimal('discount', 8, 2)->nullable();
            $table->decimal('cross_total', 8, 2);
            $table->decimal('commission', 8, 2)->nullable();
            $table->decimal('company_get', 8, 2)->

            $table->foreignId('agent_id')->nullable();

            $table->tinyInteger('payment_status')->comment('1->confirm');
            $table->tinyInteger('gender')->comment('1->male, 2->female');
            $table->tinyInteger('status')->comment('0 -> Default, 1 -> Trip closed, 2 -> Trip Cancel');
            $table->tinyInteger('cancel_req')->comment('0 -> default, 1 -> request to cancel');
            $table->string('phone');
            $table->string('passenter_name');
            $table->string('email')->nullable();
            $table->string('nid_no')->nullable();
            $table->string('image')->nullable();
            $table->dateTime('booking_date');
            $table->dateTime('pay_endtime');
            $table->dateTime('cancel_endtime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_bookings');
    }
}
