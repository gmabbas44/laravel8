<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusRegsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bus_regs', function (Blueprint $table) {
            $table->id();
            $table->string('regNo');
            $table->string('engineNo');
            $table->string('modelNo');
            $table->string('chasisNo');
            $table->string('layout');
            $table->integer('totalSeat');
            $table->string('lastSeat');
            $table->text('seatNumbers');
            $table->foreignId('coach_type_id');
            $table->string('coach_facility_id');
            $table->foreignId('company_id');
            $table->string('owner');
            $table->tinyInteger('ac_available')->comment('0->NO, 1->Yes');
            $table->tinyInteger('soft_delete')->default(0)->comment('0->NO, 1->Yes');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bus_regs');
    }
}
