<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripAssignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_assigns', function (Blueprint $table) {
            $table->id();
            $table->integer('id_no');
            $table->foreignId('bus_reg_id');
            $table->foreignId('company_id');
            $table->foreignId('trip_route_id');
            $table->string('startPoint');
            $table->string('endPoint');
            $table->string('coachNo');
            $table->dateTime('startDate');
            $table->dateTime('endDate');
            $table->integer('soldTicket');
            $table->integer('totalTicket');
            $table->decimal('totalIncome', 8, 2);
            $table->decimal('totalExpense', 8, 2);
            $table->decimal('totalFuel', 8, 2);
            $table->text('comment')->nullable();
            $table->tinyInteger('status')->comment('0->Deactive, 1->Active, 2->close');
            $table->tinyInteger('soft_delete')->default(0)->comment('0->no, 1->yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_assigns');
    }
}
