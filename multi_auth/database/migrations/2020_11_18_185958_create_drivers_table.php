<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->id();
            $table->string('driverName');
            $table->string('driverPhone');
            $table->string('nidNo')->nullable();
            $table->string('drivingLicenseNo')->nullable();
            $table->date('joinDate')->nullable();
            $table->date('leaveDate')->nullable();
            $table->foreignId('company_id');
            $table->tinyInteger('status');
            $table->tinyInteger('soft_delete')->default(0)->comment('0->no, 1->yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
