<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_prices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id');
            $table->foreignId('trip_route_id');
            $table->foreignId('coach_type_id');
            $table->string('endPointOf');
            $table->decimal('price', 8, 2)->nullable();
            $table->tinyInteger('soft_delete')->default(0)->comment('0->no, 1->yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_prices');
    }
}
