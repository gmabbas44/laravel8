<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_routes', function (Blueprint $table) {
            $table->id();
            $table->string('routeName');
            $table->foreignId('startLocationId');
            $table->string('startLocationName');
            $table->foreignId('endLocationId');
            $table->string('endLocationName');
            $table->string('endPointOf');
            $table->text('stoppage')->nullable();
            $table->string('distance')->nullable();
            $table->string('approximateTime')->nullable();
            $table->foreignId('company_id');
            $table->tinyInteger('status')->comment('0->no, 1->yes');
            $table->tinyInteger('soft_delete')->default(0)->comment('0->no, 1->yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_routes');
    }
}
