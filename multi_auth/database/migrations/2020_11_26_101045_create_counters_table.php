<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counters', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id');
            $table->foreignId('location_id');
            $table->string('counterName');
            $table->text('address');
            $table->tinyInteger('status')->comment('0->no, 1->yes');
            $table->tinyInteger('soft_delete')->default(0)->comment('0->no, 1->yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counters');
    }
}
