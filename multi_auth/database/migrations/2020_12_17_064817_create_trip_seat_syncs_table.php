<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripSeatSyncsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_seat_syncs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('agent_id')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('trip_assign_id');
            $table->string('seat_number');
            $table->tinyInteger('status')->comment('1->selected, 2->booked, 3->male confirm, 4->female confirm');
            $table->timestamp('seat_time_interval');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_seat_syncs');
    }
}
