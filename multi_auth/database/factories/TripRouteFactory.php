<?php

namespace Database\Factories;

use App\Models\TripRoute;
use Illuminate\Database\Eloquent\Factories\Factory;

class TripRouteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TripRoute::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'routeName'         => 'Chattogram-Feni',
            'startLocationId'   => 1,
            'startLocationName' => 'Chattogram',
            'endLocationId'     => 4,
            'endLocationName'   => 'Feni',
            'endPointOf'        => '1,2,3',
            'stoppage'          => 'Chattogram-Feni',
            'company_id'        => 2,
            'status'            => 1,
        ];
    }
}
