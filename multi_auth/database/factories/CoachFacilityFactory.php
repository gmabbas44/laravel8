<?php

namespace Database\Factories;

use App\Models\Coachfacility;
use Illuminate\Database\Eloquent\Factories\Factory;

class CoachFacilityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Coachfacility::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => 'wifi',
            'details' => 'Free wifi',
            'status' => 1,
            'company_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
