<?php

namespace Database\Factories;

use App\Models\Driver;
use Illuminate\Database\Eloquent\Factories\Factory;

class DriverFactory extends Factory
{
    protected $model = Driver::class;

    public function definition()
    {
        return [
            'driverName' => 'Shahed',
            'driverPhone' => '01981781661',
            'nidNo' => '1234567890',
            'drivingLicenseNo' => '123BCKD582EKE',
            'joinDate' => now(),
            'company_id' => 2,
            'status' => 1,
        ];
    }
}
