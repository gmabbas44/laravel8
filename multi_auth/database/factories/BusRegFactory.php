<?php

namespace Database\Factories;

use App\Models\BusReg;
use Illuminate\Database\Eloquent\Factories\Factory;

class BusRegFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BusReg::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
           'regNo' => 'S-102030',
           'engineNo' => '142536',
           'modelNo' => 'toyoto-504263',
           'chasisNo' => '425369',
           'layout' => '2-2',
           'totalSeat' => '50',
           'lastSeat' => '1',
           'seatNumbers' => 'A1, A2, A3, A4, B1, B2, B3, B4, C1, C2, C3, C4',
           'coach_type_id' => 1,
           'coach_facility_id' => 1,
           'company_id' => 1,
           'owner' => 'Saidul Islam Ratul',
           'ac_available' => 1,
           'created_at' => now(),
           'updated_at' => now()
        ];
    }
}
