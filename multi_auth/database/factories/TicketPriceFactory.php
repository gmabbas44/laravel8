<?php

namespace Database\Factories;

use App\Models\TicketPrice;
use Illuminate\Database\Eloquent\Factories\Factory;

class TicketPriceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TicketPrice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'company_id' => 2,
            'trip_route_id' => 1,
            'coach_type_id' => 5,
            'price' => 400.00,
        ];
    }
}
