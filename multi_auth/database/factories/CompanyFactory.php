<?php

namespace Database\Factories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'companyName' => 'Shadin Travels',
            'companyEmail' => 'shadintravel@user.com',
            'mobile' => '01854109044',
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
