<?php

namespace Database\Factories;

use App\Models\TripAssign;
use Illuminate\Database\Eloquent\Factories\Factory;

class TripAssignFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TripAssign::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // return [
        //     'id_no'         => time(),
        //     'bus_reg_id'    => 5,
        //     'company_id'    => 3,
        //     'trip_route_id' => 3,
        //     'startPoint'    => ,
        //     'endPoint'      => '',
        //     'coachNo'       => '',
        //     'startDate'     => '',
        //     'endDate'       => '',
        // ];
    }
}
