<?php

namespace Database\Factories;

use App\Models\CoachType;
use Illuminate\Database\Eloquent\Factories\Factory;

class CoachTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CoachType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => 'Ac',
            'details' => 'This is ac bus',
            'status' => '1',
            'company_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
