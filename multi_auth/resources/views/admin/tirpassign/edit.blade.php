@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection()

@section('main_content')

<h1 class="h3 mb-4 text-gray-800">{{ __('Assign Trip') }}
    <a class="btn btn-primary float-right" href="{{ route('trip-assign.index') }}">Back</a>
</h1>

<div class="card">
    <div class="card-body">
        @if( $msg = Session::get('warning') )
        <div class="alert alert-danger" role="alert">
            {{ $msg }}
        </div>
        @endif

        <form method="POST" action="{{ route('trip-assign.update') }}">
            @csrf
            <input type="hidden" name="id" value="{{$tps->id}}">
            <div class="form-row">

                <div class="form-group col-md-6">
                    <label for="busRegInfo">Fleet Reg No</label>
                    <select name="bus_reg_id" id="busRegInfo" class="form-control">
                        
                    <option value="">Select one</option>
                        @foreach( $busInfo as $bi)
                        <?php $bi->id === encryptIt($tps->bus_reg_id) ? $selected = 'selected' : $selected = '' ;?>
                        <option {{ $selected }} value="{{ $bi->id }}">{{ $bi->regNo }}</option>
                        @endforeach
                    </select>
                    <small id='alert_company' class="form-text text-danger"></small>
                    @error('bus_reg_id')
                    <small id="bus_reg_id" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="trip_route">Route name</label>
                    <select name="trip_route_id" id="trip_route" class="form-control" onchange="tirpRoute(this.value)">
                    @foreach( $routeInfo as $ri)
                    <?php $ri->id === encryptIt($tps->trip_route_id) ? $selected = 'selected' : $selected = '' ;?>
                        <option {{ $selected }} value="{{ $ri->id }}">{{ $ri->routeName }}</option>
                    @endforeach
                    </select>
                    <small id='alert_empty_route' class="form-text text-danger"></small>
                    @error('trip_route_id')
                    <small id="trip_route_id" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="startPoint">Start Point</label>
                    <select name="startPoint" id="startPoint" class="form-control">
                        <option value="{{ $tps->startPoint }}">{{ $tps->startPoint }}</option>
                    </select>
                    <small id='alert_company' class="form-text text-danger"></small>
                    @error('startPoint')
                    <small id="startPoint" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="endPoint">End Point</label>
                    <select name="endPoint" id="endPoint" class="form-control">
                    <option value="{{ $tps->endPoint }}">{{ $tps->endPoint }}</option>
                    </select>
                    <small id='alert_company' class="form-text text-danger"></small>
                    @error('endPoint')
                    <small id="endPoint" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            
                <div class="form-group col-md-6">
                    <label for="startDate">Start Date</label>
                    <input type="datetime-local" value="{{ $tps->startDate }}" name="startDate" id="startDate" class="form-control">
                    @error('startDate')
                    <small id="startDate" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="endDate">End Date</label>
                    <input type="datetime-local" value="{{ $tps->endDate }}" name="endDate" id="endDate" class="form-control">
                    @error('endDate')
                    <small id="endDate" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="">Select one</option>
                        <option value="1">Active</option>
                        <option value="0">Deactive</option>
                    </select>
                    @error('status')
                    <small id="status" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>


@endsection()


@section('custom_js')
<!-- seript here -->
<script>
    $(function() {
        $("#stoppage").select2();
    });

    function getCompanylocation(id) {
        let company_id = id;
        let busRegInfo = `<option value=''>Select One</option>`;
        let routeInfo = `<option value=''>Select One</option>`;

        // let bus = `<option value=''>Select One</option>`;
        if (company_id !== null) {
            $.ajax({
                url: "{{ route('admin.companyBusInfo')}}",
                type: 'POST',
                dataType: 'json',
                data: {
                    _token: '{{ csrf_token() }}',
                    company_id: company_id
                },
                success: function(r) {
                    let busInfo = r.businfo;
                    let routeinfo = r.routeinfo;
                    
                    if (busInfo) {
                        for (const i in busInfo) {
                            // console.log(busInfo[i]);
                            busRegInfo +=`<option value='${busInfo[i].id}'>${busInfo[i].regNo}</option>`;
                        }
                    } else {
                        document.getElementById('routemsg').innerHTML = "Please insert location first";
                    }

                    if (routeinfo) {
                        for (const i in routeinfo) {
                            // console.log(busInfo[i]);
                            routeInfo +=`<option value='${routeinfo[i].id}'>${routeinfo[i].routeName}</option>`;
                        }
                    } else {
                        document.getElementById('routemsg').innerHTML = "Please insert location first";
                    }
                    // console.log(busRegInfo);
                    document.getElementById('busRegInfo').innerHTML = busRegInfo;
                    document.getElementById('trip_route').innerHTML = routeInfo;
                }
            });

        } else {
            company_select.innerHTML = 'Select Company';
        }
    }

    function tirpRoute(id) {
        let  alertEmptyReout = document.getElementById('alert_empty_route');

        alertEmptyReout.innerHTML = '';

        const tirpRouteId = id;
        if( tirpRouteId !== '' ) {
            $.ajax({
                url: "{{ route('admin.getComanyRoute')}}",
                type: 'POST',
                dataType: 'json',
                data: {
                    _token: '{{ csrf_token() }}',
                    tirpRouteId : tirpRouteId
                },
                success : function (r) {

                    let sp  = r.startPoint;
                    let ep    = r.endPoint;
                    let startPoint = `<option value=''>Select One</option>`;
                    let endPoint = `<option value=''>Select One</option>`;
                    console.log(r);
                    for ( const i in sp) {
                        startPoint +=`<option value='${sp[i].counterName}'>${sp[i].counterName}</option>`;
                    }
                    for ( const i in ep ) {
                        endPoint +=`<option value='${ep[i].counterName}'>${ep[i].counterName}</option>`;
                    }
                    document.getElementById('startPoint').innerHTML = startPoint ;
                    document.getElementById('endPoint').innerHTML = endPoint ;
                }
            });

        }else {
            alertEmptyReout.innerHTML = 'Please Select Route name';
        }
    }

</script>

@endsection()