@extends('admin.layouts.app')

@section('style')
<!-- style here -->

@endsection

@section('main_content')
<h1 class="h3 mb-4 text-gray-800">{{ __('Trip Assign list') }}
    <a class="btn btn-primary float-right" href="{{ route('trip-assign.create')}}">Add New</a>
</h1>
@if( $msg = Session::get('success') )
<div class="alert alert-primary" role="alert">
    {{ $msg }}
</div>
@endif
<div class="card">

    <div class="card-body">
        <table class="table table-hover table-sm">
            <thead>
                <tr>
                    <th scope="col">Sl</th>
                    <th scope="col">Id No</th>
                    <th scope="col">REG No</th>
                    <th scope="col">Route Name</th>
                    @if( is_super_admin() )
                    <th scope="col">Company Name</th>
                    @endif
                    <th scope="col">Trip Start Date</th>
                    <th scope="col">Trip End Date</th>
                    <th scope="col">Status</th>
                    @if( is_super_admin() )
                    <th scope="col">Is Deleted</th>
                    @endif
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0; ?>
                @foreach( $allTripAssign as $ta )
                <?php $i++; ?>
                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $ta->id_no }}</td>
                    <td>{{ $ta->coachNo }}</td>
                    <td>{{ $ta->routeName }}</td>
                    @if( is_super_admin() )
                    <td>{{ $ta->companyName }}</td>
                    @endif
                    <td>{{ $ta->startDate }}</td>
                    <td>{{ $ta->endDate }}</td>
                    <td>{{ status($ta->status) }}</td>
                    @if( is_super_admin() )
                    <td>{{ is_deleted($ta->soft_delete) }}</td>
                    @endif
                    
                    <td>
                    <!-- <form action="Post"></form> -->

                        <a class="text-primary" href="{{ route('trip.assign.edit', ['id' => $ta->id]) }}"><i class="fa fa-edit "></i></a>
                        ||
                        <a class="text-danger"  id="delete-route" data-toggle="modal" data-target="#deleteroute" url="{{ route('admin.route.delete',['id' => $ta->id])}}"><i class="fa fa-trash "></i></a>

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>



<!-- temporary delete bus -->
<div class="modal fade" id="deleteroute" tabindex="-1" role="dialog" aria-labelledby="deleteroute" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Delete Bus Information?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Delete carefully, You are not retrieve Bus Information.</div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary text-white" id="route-delete-btn">Delete</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<!-- seript here -->

<script>

    $(function() {
        $('#delete-route').click(function() {
            let id = $(this).attr('url');
            $('#route-delete-btn').attr({
                href: id
            });
        });
    });
</script>

@endsection