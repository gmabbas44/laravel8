@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection()

@section('main_content')

<h1 class="h3 mb-4 text-gray-800">{{ __('Update Counter') }}
    <a class="btn btn-primary float-right" href="{{ route('counter.index') }}">Back</a>
</h1>

<div class="card">
    <div class="card-body">
        @if( $msg = Session::get('warning') )
        <div class="alert alert-danger" role="alert">
            {{ $msg }}
        </div>
        @endif
        <form method="POST" action="{{ route('counter.update', $counter->id) }}">
            @csrf
            @method('PATCH')
            <div class="form-row">

                <div class="form-group col-md-6">
                    <label for="location">Location</label>
                    <select name="location_id" id="location" class="form-control">
                        @foreach ( $company_locations as $location )
                        <?php $counter->location_id === $location ? $selected = 'selected' : $selected = '';?>
                        <option {{ $selected }} value="{{$location->id}}">{{ $location->locationName }}</option>
                        @endforeach
                    </select>
                    @error('location')
                    <small id="location" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="counterName">Counter Name</label>
                    <input type="text" value="{{ $counter->counterName }}" name="counterName" class="form-control" id="counterName" placeholder="Enter Driver Name">
                    @error('counterName')
                    <small id="counterName" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="">Select one</option>
                        <option value="1">Active</option>
                        <option value="0">Deactive</option>
                    </select>
                    @error('status')
                    <small id="status" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-12">
                    <label for="address">Address</label>
                    <textarea name="address" id="address" cols="30" rows="5" class="form-control">{{ $counter->address }}</textarea>
                </div>
                @error('address')
                <small id="address" class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>


@endsection()


@section('custom_js')
<!-- seript here -->
<script>

    
</script>

@endsection()