@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection()

@section('main_content')

<h1 class="h3 mb-4 text-gray-800">{{ __('Add new Counter') }}
    <a class="btn btn-primary float-right" href="{{ route('counter.index') }}">Back</a>
</h1>

<div class="card">
    <div class="card-body">
        @if( $msg = Session::get('warning') )
        <div class="alert alert-danger" role="alert">
            {{ $msg }}
        </div>
        @endif

        <form method="POST" action="{{ route('counter.store') }}">
            @csrf
            <div class="form-row">

                @if( is_super_admin() )
                <div class="form-group col-md-6">
                    <label for="company">Company</label>
                    <select name="company" id="company" class="form-control" onchange="getloacationByCompany(this.value)">
                        <option value="">Select one</option>
                        @foreach( $companies as $company)
                        <option value="{{ $company->id }}">{{ $company->companyName }}</option>
                        @endforeach
                    </select>
                </div>
                @endif

                <div class="form-group col-md-6">
                    <label for="location">Location</label>
                    <select name="location" id="location" class="form-control">
                        @foreach ( $locations as $location )
                        <option value="{{$location->id}}">{{ $location->locationName }}</option>
                        @endforeach
                    </select>
                    @error('location')
                    <small id="location" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="counterName">Counter Name</label>
                    <input type="text" value="{{ old('counterName') }}" name="counterName" class="form-control" id="counterName" placeholder="Enter Counter Name">
                    @error('counterName')
                    <small id="counterName" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="">Select one</option>
                        <option value="1">Active</option>
                        <option value="0">Deactive</option>
                    </select>
                    @error('status')
                    <small id="status" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-12">
                    <label for="address">Address</label>
                    <textarea name="address" id="address" cols="30" rows="5" class="form-control"></textarea>
                </div>
                @error('address')
                <small id="address" class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>


@endsection()


@section('custom_js')
<!-- seript here -->

<script>

    <?php if( !is_super_admin() ) : ?>
        getloacationByCompany( "{{ Auth::user()->company_id }}")
    <?php endif; ?>


    function getloacationByCompany(id) {
        let company_id = id;

        let location_name = `<option value=''>Select One</option>`;
        let location_value = '';
        if (company_id !== null) {
            $.ajax({
                url: "{{ route('admin.get.company.route.info')}}",
                type: 'POST',
                dataType: 'json',
                data: {
                    _token: '{{ csrf_token() }}',
                    company_id: company_id
                },
                success: function(r) {
                    d = r.routes;
                    console.log(d);
                    if (d) {
                        for (const i in d) {
                            location_name += `<option value='${d[i].id}'>${d[i].locationName}</option>`;
                        }
                    }
                    // document.getElementById("routeName").innerHTML = location_name;
                    document.getElementById('location').innerHTML = location_name;
                }
            });

        }
    }
</script>

@endsection()