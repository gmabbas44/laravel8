@extends('admin.layouts.app')

@section('style')
<!-- style here -->

@endsection

@section('main_content')
<h1 class="h3 mb-4 text-gray-800">{{ __('Counter List') }}
    <a class="btn btn-primary float-right" href="{{ route('counter.create')}}">Add New</a>
</h1>
@if( $msg = Session::get('success') )
<div class="alert alert-primary" role="alert">
    {{ $msg }}
</div>
@endif
<div class="card">
    <div class="card-body">

        <table class="table table-hover table-sm">
            <thead>
                <tr>
                    <th scope="col">Sl</th>
                    <th scope="col">Counter Name</th>
                    @if( is_super_admin() )
                    <th scope="col">Company Name</th>
                    @endif
                    <th scope="col">Location Name</th>
                    <th scope="col">Address</th>
                    <th scope="col">Status</th>
                    <@if( is_super_admin() )
                    <th scope="col">Is Deleted</th>
                    @endif
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0; ?>
                @foreach( $counters as $c )
                <?php $i++; ?>
                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $c->counterName }}</td>
                    @if( is_super_admin() )
                    <td>{{ $c->companyName }}</td>
                    @endif
                    <td>{{ $c->locationName }}</td>
                    <td>{{ $c->address }}</td>
                    <td>{{ status($c->status) }}</td>
                    <td>{{ is_deleted($c->soft_delete) }}</td>
                    <td>
                        <a class="text-primary" href="{{ route('counter.edit', $c->id) }}"><i class="fa fa-edit "></i></a>
                        ||
                        <a class="text-danger" id="delete-driver" data-toggle="modal" data-target="#deleteDriver" url="{{ __('this ') }}"><i class="fa fa-trash "></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>



<!-- temporary delete bus -->
<div class="modal fade" id="deleteDriver" tabindex="-1" role="dialog" aria-labelledby="deleteDriver" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Delete Bus Information?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Delete carefully, You are not retrieve Bus Information.</div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary text-white" id="driver-delete-btn">Delete</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<!-- seript here -->

<script>
    $(function() {
        $('#delete-driver').click(function() {
            let id = $(this).attr('url');
            $('#driver-delete-btn').attr({
                href: id
            });
        });
    });
</script>

@endsection