@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection()

@section('main_content')

<h1 class="h3 mb-4 text-gray-800">{{ __('Edit Bus information') }}
    <a class="btn btn-primary float-right" href="{{ route('admin.bus.list') }}">Back</a>
</h1>

<div class="card">
    <div class="card-body">
        @if( $msg = Session::get('warning') )
        <div class="alert alert-danger" role="alert">
            {{ $msg }}
        </div>
        @endif

        <form method="POST" action="{{ route('admin.bus.update')}}">
            @csrf
            <input type="hidden" name="id" value="{{$getbusinfo->id }}">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="regNo">Registration</label>
                    <input type="text" value="{{ $getbusinfo->regNo }}" name="regNo" class="form-control" id="regNo" placeholder="Enter Bus Registration">
                    @error('regNo')
                    <small id="regNo" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="engineNo">Engine No</label>
                    <input type="text" value="{{ $getbusinfo->engineNo }}" name="engineNo" class="form-control" id="engineNo" placeholder="Enter Bus Engine NO">
                    @error('engineNo')
                    <small id="engineNo" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="modelNo">Model No</label>
                    <input type="text" value="{{ $getbusinfo->modelNo }}" name="modelNo" class="form-control" id="modelNo" placeholder="Enter Bus Model No">
                    @error('modelNo')
                    <small id="modelNo" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-3">
                    <label for="chasisNo">Chasis No</label>
                    <input type="text" value="{{ $getbusinfo->chasisNo }}" name="chasisNo" class="form-control" id="chasisNo" placeholder="Enter Bus Chasis No">
                    @error('chasisNo')
                    <small id="chasisNo" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-3">
                    <label for="layout">Bus Layout</label>
                    <select class="form-control" name="layout" id="layout">
                        <option value="">Select One</option>
                        @foreach( $layouts as $layout )
                        <?php  $layout == $getbusinfo->layout ? $selected = 'selected' : $selected = ''  ; ?>
                        <option {{ $selected }} value="{{ $layout }}">{{ $layout }}</option>
                        @endforeach
                    </select>
                    @error('layout')
                    <small id="layout" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-3">
                    <label for="totalSeat">Total Seat</label>
                    <input type="number" min="10" value="{{ $getbusinfo->totalSeat }}" name="totalSeat" class="form-control" id="totalSeat" placeholder="Enter Bus Chasis No">
                    @error('totalSeat')
                    <small id="totalSeat" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-3">
                    <label for="lastSeat">Last Seat Availabe</label>
                    <select class="form-control" name="lastSeat" id="lastSeat">
                        
                        <option value="0">NO</option>
                        <option value="1">Yes</option>
                    </select>
                    @error('lastSeat')
                    <small id="lastSeat" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="from-group col-md-12">
                    <label for="seatNumbers">Seat Number</label>
                    <textarea class="form-control" name="seatNumbers" id="seatNumbers" cols="30" rows="3">{{ $getbusinfo->seatNumbers }}</textarea>
                    <small id="lastSeat" class="form-text text-danger">*Use comma to separate the seat number, like A1, A2,</small>
                    @error('seatNumbers')
                    <small id="seatNumbers" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                @if( is_super_admin() )
                <div class="form-group col-md-4">
                    <label for="company">Company</label>
                    <select class="form-control" name="company" id="company" onchange="loadCompany(this.value)">
                        <option value="">Select Company</option>
                        @foreach( $companies as $c )
                        <?php  $c->id == $getbusinfo->company_id ? $selected = 'selected' : $selected = ''  ; ?>
                        <option {{ $selected }} value="{{ $c->id }}">{{ $c->companyName }}</option>
                        @endforeach
                    </select>
                    <small id="alert_company" class="form-text text-danger"></small>
                    @error('company')
                    <small id="company" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                @endif


                <div class="form-group col-md-4">
                    <label for="coach_type">Coach Type</label>
                    <select class="form-control" name="coach_type" id="coach_type">
                        <option value="">Select Coach type</option>
                        @if( $coach_types )
                        @foreach( $coach_types as $type )
                        <?php  $type->id == $getbusinfo->coach_type_id ? $selected = 'selected' : $selected = ''  ; ?>
                        <option {{ $selected }} value="{{ $type->id }}">{{ $type->title}}</option>
                        @endforeach
                        @endif
                    </select>
                    @error('coach_type')
                    <small id="coach_type" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="coach_facility">Coach Facility</label>
                    <select class="form-control" name="coach_facility" id="coach_facility">
                        <option value="">Select Coach facility</option>
                        @if( $coach_facilities)
                        @foreach( $coach_facilities as $cf )
                        <?php  $cf->id == $getbusinfo->coach_facility_id ? $selected = 'selected' : $selected = ''  ; ?>
                        <option {{ $selected }} value="{{ $cf->id }}">{{ $cf->title }}</option>
                        @endforeach
                        @endif
                    </select>
                    @error('coach_facility')
                    <small id="coach_facility" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>


                <div class="form-group col-md-4">
                    <label for="owner">Owner</label>
                    <input type="text" class="form-control" name="owner" id="owner" value="{{ $getbusinfo->owner }}" placeholder="Enter Bus owner Name">
                    @error('owner')
                    <small id="owner" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="ac_available">AC Availabe</label>
                    <select class="form-control" name="ac_available" id="ac_available">
                        <option value="1">Yes</option>
                        <option value="0">NO</option>
                    </select>
                    @error('ac_available')
                    <small id="ac_available" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="status">Status</label>
                    <select class="form-control" name="status" id="status">
                        <option value="1">Yes</option>
                        <option value="0">NO</option>
                    </select>
                    @error('status')
                    <small id="status" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>


@endsection()


@section('custom_js')
<!-- seript here -->
<script>
    function loadCompany(id) {

        let company_id = id,
            i, j, coach_type = '',
            coach_facility = '';

        $('#alert_company').text(null);
        if (company_id !== null) {
            $.ajax({
                dataType: 'json',
                url: "{{ route('admin.get.company.facility') }}",
                type: "POST",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: company_id
                },
                success: function(r) {

                    const ct = r.ct;
                    const cf = r.cf;

                    // for( const i in  r ) {
                    //     coach_type += `<option value='${r.ct[i].id}'>${r.ct[i].title}</option>`;
                    //     coach_facility += `<option value='${r.cf[i].id}'>${r.cf[i].title}</option>`;
                    // }

                    for (const i in ct) {
                        coach_type += `<option value='${ct[i].id}'>${ct[i].title}</option>`;
                    }
                    for (const j in cf) {
                        coach_facility += `<option value='${cf[j].id}'>${cf[j].title}</option>`;
                    }
                    $('#coach_type').html(coach_type);
                    $('#coach_facility').html(coach_facility);

                    // console.log(coach_type);
                }
            });
        } else {
            $('#alert_company').text('Select Company');
        }
    }
</script>
@endsection()