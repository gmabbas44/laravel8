@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection()

@section('main_content')

<h1 class="h3 mb-4 text-gray-800">{{ __('Add Ticket Price') }}
    <a class="btn btn-primary float-right" href="{{ route('ticket-price.index') }}">Back</a>
</h1>

<div class="card">
    <div class="card-body">
        @if( $msg = Session::get('warning') )
        <div class="alert alert-danger" role="alert">
            {{ $msg }}
        </div>
        @endif

        <form method="POST" action="{{ route('ticket-price.store') }}">
            @csrf
            <div class="form-row">
                @if( is_super_admin() )
                <div class="form-group col-md-6">
                    <label for="company">Company</label>
                    <select name="company" id="company" class="form-control" onChange="getCompanylocation(this.value)">
                        <option value="">Select one</option>
                        @foreach( $companies as $company)
                        <option value="{{ $company->id }}">{{ $company->companyName }}</option>
                        @endforeach
                    </select>
                    <small id='alert_company' class="form-text text-danger"></small>
                    @error('company')
                    <small id="company" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                @endif

                <div class="form-group col-md-6">
                    <label for="trip_route">Route Name</label>
                    <select class="form-control" name="trip_route" id="trip_route" onchange="getStoppage(this.value)">
                        <option value="">Select One</option>
                        @foreach( $routeName as $name)
                        <option value="{{$name->id}}">{{ $name->routeName}}</option>
                        @endforeach
                    </select>
                    <small id='routemsg' class="form-text text-danger"></small>
                    @error('trip_route')
                    <small id="trip_route" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="coach_type">Coach Type</label>
                    <select class="form-control" name="coach_type" id="coach_type">
                        <option value="">Select One</option>
                        @foreach( $coachTypes as $coachType)
                        <option value="{{ $coachType->id }}">{{ $coachType->title }}</option>
                        @endforeach
                    </select>
                    <small id='coachtypemsg' class="form-text text-danger"></small>
                    @error('coach_type')
                    <small id="coach_type" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="stoppage">Droping Point</label>
                    <select class="form-control" name="endPointOf" id="endPointOf">
                        <!-- <option value="">Select One</option> -->
                        
                    </select>
                    <small id='stoppageErrmsg' class="form-text text-danger"></small>
                    @error('stoppage')
                    <small id="stoppage" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="price">Ticket Pirce</label>
                    <div class="input-group">
                        <input type="number" id="price" class="form-control" name="price">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">BDT</span>
                        </div>
                    </div>
                    @error('price')
                    <small id="price" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>


@endsection()


@section('custom_js')
<!-- seript here -->
<script>

    function getCompanylocation(id) {
        let company_id = id,
            company_select, routemsg, coachtypemsg;

        company_select = document.getElementById('alert_company');
        routemsg = document.getElementById('routemsg');
        coachtypemsg = document.getElementById('coachtypemsg');

        company_select.textContent = '';
        routemsg.innerHTML = '';
        coachtypemsg.innerHTML = '';

        let route_name = `<option value=''>Select One</option>`;
        let coach_type = `<option value=''>Select One</option>`;

        if (company_id !== '') {
            $.ajax({
                url: "{{ route('getCompanyRouteCoach')}}",
                type: 'POST',
                dataType: 'json',
                data: {
                    _token: '{{ csrf_token() }}',
                    company_id: company_id
                },
                success: function(r) {
                    coachTypes = r.coachTypes;
                    routeName = r.routeName;

                    if (coachTypes.length !== 0) {
                        for (const i in coachTypes) {
                            coach_type += `<option value='${coachTypes[i].id}'>${coachTypes[i].title}</option>`;
                        }
                    } else {
                        coachtypemsg.innerHTML = `Please insert coach route first <a href="{{route('admin.add.coach.type')}}">Go</a>`;
                    }

                    if (routeName.length !== 0) {
                        for (const i in routeName) {
                            route_name += `<option value='${routeName[i].id}'>${routeName[i].routeName}</option>`;
                        }
                    } else {
                        routemsg.innerHTML = `Please insert coach type first <a href="{{route('admin.route.create')}}">Go</a>`;
                    }
                    document.getElementById("coach_type").innerHTML = coach_type;
                    document.getElementById("trip_route").innerHTML = route_name;
                }
            });

        } else {
            company_select.textContent = 'Select Company';
            document.getElementById("coach_type").innerHTML = coach_type;
            document.getElementById("trip_route").innerHTML = route_name;
        }
    }

    function getStoppage(id){
        let route_id = id, endPointOf, routemsg, stoppage, stoppageErrmsg;

        routemsg        = document.getElementById('routemsg');
        endPointOf    = document.getElementById('endPointOf');
        stoppageErrmsg  = document.getElementById('stoppageErrmsg');

        routemsg.textContent    = '';
        endPointOf.innerHTML  = `<option value=''>Select One</option>`;

        if ( route_id !== '' ) {
            $.ajax({
                url: "{{ route('getStoppage')}}",
                type: 'POST',
                dataType: 'json',
                data: {
                    _token: '{{ csrf_token() }}',
                    route_id: route_id
                },
                success: function(r) {
                    let endPoint = r.endPointOf;
                    if(endPoint.length !== 0 ){
                        for ( const i in endPoint) {
                            stoppage += `<option value='${endPoint[i].locationName}'>${endPoint[i].locationName}</option>`;
                        }
                    } else {
                        stoppageErrmsg.innerHTML = `Please <a href="{{ route('admin.location.create')}}">Go</a> for insert location`;
                    }
                    endPointOf.innerHTML = stoppage;
                }
            });
        } else {
            routemsg.textContent = 'Please select Route name';
        }
    }
</script>

@endsection()