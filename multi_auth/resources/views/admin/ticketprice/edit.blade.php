@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection()

@section('main_content')

<h1 class="h3 mb-4 text-gray-800">{{ __('Ticket Price Update') }}
    <a class="btn btn-primary float-right" href="{{ route('ticket-price.index') }}">Back</a>
</h1>

<div class="card">
    <div class="card-body">
        @if( $msg = Session::get('warning') )
        <div class="alert alert-danger" role="alert">
            {{ $msg }}
        </div>
        @endif

        <form method="POST" action="{{ route('ticket-price.update') }}">
            @csrf
            <input type="hidden" name="id" value="{{ $ticketPriceId->id }}">
            <div class="form-row">

                <div class="form-group col-md-6">
                    <label for="trip_route">Route Name</label>
                    <select class="form-control" name="trip_route" id="trip_route">
                        <option value="">Select One</option>
                        @foreach( $routeName as $name)
                        <?php $name->id === encryptIt($ticketPriceId->trip_route_id) ? $selected = 'selected' : $selected = ''?>
                        <option {{ $selected }} value="{{$name->id}}">{{ $name->routeName}}</option>
                        @endforeach
                    </select>
                    <small id='routemsg' class="form-text text-danger"></small>
                    @error('trip_route')
                    <small id="trip_route" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="coach_type">Coach Type</label>
                    <select class="form-control" name="coach_type" id="coach_type">
                        <option value="">Select One</option>
                        @foreach( $coachTypes as $coachType)
                        
                        <?php $coachType->id === $ticketPriceId->coach_type_id ? $selected = 'selected' : $selected = ''?>
                        <option {{ $selected }} value="{{ $coachType->id }}">{{ $coachType->title }}</option>
                        @endforeach
                    </select>
                    <small id='coachtypemsg' class="form-text text-danger"></small>
                    @error('coach_type')
                    <small id="coach_type" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="price">Ticket Pirce</label>
                    <div class="input-group">
                        <input type="number" id="price" class="form-control" name="price" value="{{ $ticketPriceId->price}}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">BDT</span>
                        </div>
                    </div>
                    @error('price')
                    <small id="price" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>


@endsection()


@section('custom_js')
<!-- seript here -->
<script>
    $(function() {
        $("#stoppage").select2();
    });

    function getCompanylocation(id) {
        let company_id = id,
            company_select, routemsg, coachtypemsg;

        company_select = document.getElementById('alert_company');
        routemsg = document.getElementById('routemsg');
        coachtypemsg = document.getElementById('coachtypemsg');

        company_select.textContent = '';
        routemsg.textContent = '';
        coachtypemsg.textContent = '';

        let route_name = `<option value=''>Select One</option>`;
        let coach_type = `<option value=''>Select One</option>`;

        if (company_id !== '') {
            $.ajax({
                url: "{{ route('getCompanyRouteCoach')}}",
                type: 'POST',
                dataType: 'json',
                data: {
                    _token: '{{ csrf_token() }}',
                    company_id: company_id
                },
                success: function(r) {
                    coachTypes = r.coachTypes;
                    routeName = r.routeName;

                    if (coachTypes.length !== 0) {
                        for (const i in coachTypes) {
                            coach_type += `<option value='${coachTypes[i].id}'>${coachTypes[i].title}</option>`;
                        }
                    } else {
                        coachtypemsg.innerHTML = `Please insert coach route first <a href="{{route('admin.add.coach.type')}}">Go</a>`;
                    }

                    if (routeName.length !== 0) {
                        for (const i in routeName) {
                            route_name += `<option value='${routeName[i].id}'>${routeName[i].routeName}</option>`;
                        }
                    } else {
                        routemsg.innerHTML = `Please insert coach route first <a href="{{route('admin.route.create')}}">Go</a>`;
                    }
                    document.getElementById("coach_type").innerHTML = coach_type;
                    document.getElementById("trip_route").innerHTML = route_name;

                }
            });

        } else {
            company_select.innerHTML = 'Select Company';
            document.getElementById("coach_type").innerHTML = coach_type;
            document.getElementById("trip_route").innerHTML = route_name;
        }


    }
</script>

@endsection()