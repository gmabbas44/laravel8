@extends('admin.layouts.app')

@section('style')
<!-- style here -->

@endsection

@section('main_content')
<h1 class="h3 mb-4 text-gray-800">{{ __('Trip Search list') }}</h1>

<div class="card">
    <div class="card-header">
        <form class="form-inline float-left">
            <label class="sr-only" for="startLocation">From</label>
            <input type="text" name="startLocation" class="form-control form-control-sm mb-2 mr-sm-2" id="startLocation" placeholder="From">

            <label class="sr-only" for="endLocation">To</label>
            <input type="text" name="endLocation" class="form-control form-control-sm mb-2 mr-sm-2" id="endLocation" placeholder="To">

            <label class="sr-only" for="bookingDate">Date</label>
            <input type="datetime-local" name="bookingDate" class="form-control form-control-sm mb-2 mr-sm-2" id="bookingDate" placeholder="Booking Date">

            <button type="submit" class="btn btn-sm btn-primary mb-2"> <i class="fa fa-search"></i> Submit</button>
        </form>
        <button class="btn btn-sm btn-primary float-right" onclick="refresh()"><i class="fas fa-redo"></i> Refresh</button>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover table-sm">
                <thead class="thead" >
                    <th>Company Name</th>
                    <th>Route</th>
                    <th>Start Point</th>
                    <th>End Point</th>
                    <th>Departure</th>
                    <th>Duration</th>
                    <th>Distance</th>
                    <th>Total Seat</th>
                    <th>Fare</th>
                    <th>View Seats</th>
                </thead>
                <tbody id="assignList">
                    
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('custom_js')
<!-- seript here -->

<script>
    function refresh() {
        location.reload();
    }
    tirplist();

    function tirplist(start = null, end = null, booking = null) {
        let startLocation = start,
            endLocation = end,
            bookingDate = booking,
            list;

        let assignListTable = document.getElementById('assignList');
        list = '';

        $.ajax({
            url: "{{ route('gettriplist') }}",
            type: 'POST',
            data: {
                _token: '{{ csrf_token() }}',
                startLocation   : startLocation,
                endLocation     : endLocation,
                bookingDate     : bookingDate
            },
            success: function(assignlist) {

                if (assignlist.length > 0) {

                    for ( const i of assignlist ) {
                        list += `<tr>
                                <td>${i.companyName}</td>
                                <td>${i.routeName} <br> ${i.startDate}</td>
                                <td>${i.startPoint}</td>
                                <td>${i.endPoint}</td>
                                <td>${i.approximateTime}</td>
                                <td>${i.approximateTime}</td>
                                <td>${i.distance}</td>
                                <td>${i.totalSeat}</td>
                                <td>${i.price}</td>
                                <td><a title="Seat View" href="{{ route('admin.seat.view', '') }}/${i.id} "><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                            </tr>`;
                    }
                    assignListTable.innerHTML = list;
                }
            }
        });

    }
</script>

@endsection