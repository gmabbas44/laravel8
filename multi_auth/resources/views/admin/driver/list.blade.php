@extends('admin.layouts.app')

@section('style')
<!-- style here -->

@endsection

@section('main_content')
<h1 class="h3 mb-4 text-gray-800">{{ __('Driver List') }}
    <a class="btn btn-primary float-right" href="{{ route('admin.driver.create')}}">Add New</a>
</h1>
@if( $msg = Session::get('success') )
<div class="alert alert-primary" role="alert">
    {{ $msg }}
</div>
@endif
<div class="card">

    <div class="card-body">
        <table class="table table-hover table-sm">
            <thead>
                <tr>
                    <th scope="col">Sl</th>
                    <th scope="col">Driver Name</th>
                    <th scope="col">Phone</th>
                    <th scope="col">NID No</th>
                    <th scope="col">Driving License No</th>
                    <th scope="col">Join Date</th>
                    <th scope="col">Leave Date</th>
                    @if( is_super_admin() )
                    <th scope="col">Company Name</th>
                    @endif
                    <th scope="col">Status</th>
                    @if( is_super_admin() )
                    <th scope="col">Is Deleted</th>
                    @endif
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0; ?>
                @foreach( $drivers as $driver )
                <?php $i++; ?>
                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $driver->driverName }}</td>
                    <td>{{ $driver->driverPhone }}</td>
                    <td>{{ $driver->nidNo }}</td>
                    <td>{{ $driver->drivingLicenseNo }}</td>
                    <td>{{ $driver->joinDate }}</td>
                    <td>
                        @if( $driver->leaveDate)
                        {{ $driver->leaveDate }}
                        @else
                        {{ __('On Ride')}}
                        @endif
                    </td>
                    @if( is_super_admin() )
                    <td>{{ $driver->companyName }}</td>
                    @endif
                    <td>{{ status($driver->status) }}</td>
                    <td>{{ is_deleted($driver->soft_delete) }}</td>
                    <td>
                        <a class="text-primary" href="{{ route('admin.driver.edit', ['id' => $driver->id ]) }}"><i class="fa fa-edit "></i></a>
                        ||
                        <a class="text-danger" id="delete-driver" data-toggle="modal" data-target="#deleteDriver" url="{{ route('admin.driver.delete',['id' => $driver->id])}}"><i class="fa fa-trash "></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>



<!-- temporary delete bus -->
<div class="modal fade" id="deleteDriver" tabindex="-1" role="dialog" aria-labelledby="deleteDriver" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Delete Bus Information?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Delete carefully, You are not retrieve Bus Information.</div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary text-white" id="driver-delete-btn">Delete</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<!-- seript here -->

<script>
    $(function() {
        $('#delete-driver').click(function() {
            let id = $(this).attr('url');
            $('#driver-delete-btn').attr({
                href: id
            });
        });
    });
</script>

@endsection