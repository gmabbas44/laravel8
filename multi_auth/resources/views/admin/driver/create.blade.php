@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection()

@section('main_content')

<h1 class="h3 mb-4 text-gray-800">{{ __('Add new Driver') }}
    <a class="btn btn-primary float-right" href="{{ route('admin.driver.list') }}">Back</a>
</h1>

<div class="card">
    <div class="card-body">
    @if( $msg = Session::get('warning') )
        <div class="alert alert-danger" role="alert">
            {{ $msg }}
        </div>
    @endif

        <form method="POST" action="{{ route('admin.driver.save') }}">
            @csrf
            <div class="form-row">
                
                <div class="form-group col-md-6">
                    <label for="driverName">Driver Name</label>
                    <input type="text" value="{{ old('driverName') }}" name="driverName" class="form-control" id="driverName" placeholder="Enter Driver Name">
                    @error('driverName')
                    <small id="driverName" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="driverPhone">Driver Phone No</label>
                    <input type="number" value="{{ old('driverPhone') }}" name="driverPhone" class="form-control" id="driverPhone" placeholder="Enter Driver Phone No">
                    @error('driverPhone')
                    <small id="driverPhone" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="nidNo">Driver NID No</label>
                    <input type="text" value="{{ old('nidNo') }}" name="nidNo" class="form-control" id="nidNo" placeholder="Enter Driver NID No">
                    @error('nidNo')
                    <small id="nidNo" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="drivingLicenseNo">Driving License No</label>
                    <input type="text" value="{{ old('drivingLicenseNo') }}" name="drivingLicenseNo" class="form-control" id="drivingLicenseNo" placeholder="Enter Driving License No">
                    @error('drivingLicenseNo')
                    <small id="drivingLicenseNo" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                @if( is_super_admin() )
                <div class="form-group col-md-6">
                    <label for="company">Company</label>
                    <select name="company" id="company" class="form-control">
                        <option value="">Select one</option>
                        @foreach( $companies as $company)
                        <option value="{{ $company->id }}">{{ $company->companyName }}</option>
                        @endforeach
                    </select>
                    @error('company')
                    <small id="company" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                @endif
                <div class="form-group col-md-6">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="">Select one</option>
                        <option value="1">Active</option>
                        <option value="0">Deactive</option>
                    </select>
                    @error('status')
                    <small id="status" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>


@endsection()


@section('custom_js')
<!-- seript here -->

@endsection()