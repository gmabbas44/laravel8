@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection()

@section('main_content')

<h1 class="h3 mb-4 text-gray-800">{{ __('Add New Route') }}
    <a class="btn btn-primary float-right" href="{{ route('admin.route.list') }}">Back</a>
</h1>

<div class="card">
    <div class="card-body">
        @if( $msg = Session::get('warning') )
        <div class="alert alert-danger" role="alert">
            {{ $msg }}
        </div>
        @endif

        <form method="POST" action="{{ route('admin.route.save') }}">
            @csrf
            <div class="form-row">
                @if( is_super_admin() )
                <div class="form-group col-md-6">
                    <label for="company">Company</label>
                    <select name="company" id="company" class="form-control" onChange="getCompanylocation(this.value)">
                        <option value="">Select one</option>
                        @foreach( $companies as $company)
                        <option value="{{ $company->id }}">{{ $company->companyName }}</option>
                        @endforeach
                    </select>
                    <small id='alert_company' class="form-text text-danger"></small>
                    <small id='errLocation' class="form-text text-danger"></small>
                    @error('company')
                    <small id="company" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                @endif

                <div class="form-group col-md-6">
                    <label for="startLocation">Start Point</label>
                    <select class="form-control" name="startLocation" id="startLocation">
                        @foreach( $locations as $location )
                        <option value="{{ $location->locationName }}">{{ $location->locationName }}</option>
                        @endforeach
                    </select>
                    @error('startLocation')
                    <small id="startLocation" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="endLocation">End Point</label>
                    <select class="form-control" name="endLocation" id="endLocation">
                        @foreach( $locations as $location )
                        <option value="{{ $location->locationName }}">{{ $location->locationName }}</option>
                        @endforeach
                    </select>
                    @error('endLocation')
                    <small id="endLocation" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="routeName">Route Name</label>
                    <input type="text" id="routeName" class="form-control" name="routeName">
                    @error('routeName')
                    <small id="routeName" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="endPointOf">End Point Of</label>
                    <select name="endPointOf[]" id="endPointOf" class="form-control" multiple="multiple" >
                        <option value="">Select One</option>
                        @foreach( $locations as $location )
                        <option value="{{ $location->locationName }}">{{ $location->locationName }}</option>
                        @endforeach
                    </select>
                    @error('endPointOf')
                    <small id="endPointOf" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="stoppage">Stoppage</label>
                    <select name="stoppage[]" id="stoppage" class="form-control" multiple="multiple">
                        @foreach( $counters as $counter )
                        <option value="{{ $counter->counterName }}">{{ $counter->counterName }}</option>
                        @endforeach
                    </select>
                    <small id="stoppageErr" class="form-text text-danger"></small>
                    @error('stoppage')
                    <small id="stoppage" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="distance">Distance</label>
                    <div class="input-group">
                        <input type="number" name="distance" id="distance" class="form-control" placeholder="Distance">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">Kilometer</span>
                        </div>
                    </div>
                    @error('distance')
                    <small id="distance" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="approximateTime">Approximate Time</label>
                    <div class="input-group">
                        <input type="number" name="approximateTime" id="approximateTime" class="form-control" placeholder="Approximate Time">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">Hours</span>
                        </div>
                    </div>
                    @error('approximateTime')
                    <small id="approximateTime" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="">Select one</option>
                        <option value="1">Active</option>
                        <option value="0">Deactive</option>
                    </select>
                    @error('status')
                    <small id="status" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>


@endsection()


@section('custom_js')
<!-- seript here -->
<script>
    $(function() {
        $("#stoppage").select2()
        $("#endPointOf").select2();
    });

    function getCompanylocation(id) {
        let company_id = id;
        let company_select = document.getElementById('alert_company');
        let errLocation = document.getElementById('errLocation');
        let stoppageErr = document.getElementById('stoppageErr');

        company_select.innerHTML    = '';
        errLocation.innerHTML     = '';
        stoppageErr.innerHTML     = '';

        let location_name, endPointOf, stoppage_name;

        if (company_id !== null) {
            $.ajax({
                url: "{{ route('admin.get.company.route.info')}}",
                type: 'POST',
                dataType: 'json',
                data: {
                    _token: '{{ csrf_token() }}',
                    company_id: company_id
                },
                success: function(r) {
                    d = r.routes;
                    counter = r.counter;
                    if (d.length !== 0 ) {
                        location_name = `<option value=''>Select One</option>`;
                        endPointOf = `<option value=''>Select One</option>`;
                        for (const i in d) {
                            location_name += `<option value='${d[i].id}'>${d[i].locationName}</option>`;
                            endPointOf += `<option value='${d[i].id}'>${d[i].locationName}</option>`;
                        }
                    } else {
                        errLocation.innerHTML = `Haven't any location, Please <a href='{{ route("admin.location.create")}}'>GO</a> insert location first`;
                    }
                    if (counter.length !== 0 ) {
                        stoppage_name = `<option value=''>Select One</option>`;
                        for (const i in counter) {
                            stoppage_name += `<option value='${counter[i].counterName}'>${counter[i].counterName}</option>`;
                        }
                    } else {
                        stoppageErr.innerHTML = `Haven't any Counter, Please <a href='{{ route("counter.create")}}'>GO</a> insert location first`;
                    }

                    // document.getElementById("routeName").innerHTML = location_name;
                    document.getElementById("endPointOf").innerHTML = endPointOf;
                    document.getElementById('startLocation').innerHTML = location_name;
                    document.getElementById('endLocation').innerHTML = location_name;
                    document.getElementById('stoppage').innerHTML = stoppage_name;
                    document.getElementById('routeName').value = '';
                    $("#stoppage").select2();
                }
            });
        } else {
            company_select.innerHTML = 'Select Company';
        }
    }

    const startLocation = document.querySelector("#startLocation") ;
    startLocation.addEventListener('change',(e) => {
        // console.log(e.target.value);
        const startLocationTaxt  = e.target.options[e.target.selectedIndex].text;
        const addstartRoute = document.querySelector('#routeName');
        addstartRoute.value = startLocationTaxt !== 'Select One' ? `${startLocationTaxt} - ` :  '';
        
    });

    const endLocation = document.querySelector("#endLocation") ;
    endLocation.addEventListener('change',(e) => {
        // console.log(e.target.options[e.target.selectedIndex].text);
        const endLocationTaxt  = e.target.options[e.target.selectedIndex].text;
        const addstartRoute = document.querySelector('#routeName');
        addstartRoute.value += endLocationTaxt !== 'Select One' ? endLocationTaxt : '';
    });
    
</script>

@endsection()