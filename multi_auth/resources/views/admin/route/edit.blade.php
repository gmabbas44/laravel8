@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection()

@section('main_content')

<h1 class="h3 mb-4 text-gray-800">{{ __('Update Company Route') }}
    <a class="btn btn-primary float-right" href="{{ route('admin.route.list') }}">Back</a>
</h1>

<div class="card">
    <div class="card-body">
        @if( $msg = Session::get('warning') )
        <div class="alert alert-danger" role="alert">
            {{ $msg }}
        </div>
        @endif

        <form method="POST" action="{{ route('admin.route.save') }}">
            @csrf
            <div class="form-row">

                <div class="form-group col-md-6">
                    <label for="startLocation">Start Point</label>
                    <select class="form-control" name="startLocation" id="startLocation">
                        @foreach( $locations as $location )
                        <?php $location->id == $tr->startLocationId ? $selected = 'selected' : $selected = ''; ?>
                        <option {{ $selected}} value="{{ $location->locationName }}">{{ $location->locationName }}</option>
                        @endforeach
                    </select>
                    @error('startLocation')
                    <small id="startLocation" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="endLocation">End Point</label>
                    <select class="form-control" name="endLocation" id="endLocation">
                        @foreach( $locations as $location )
                        <option value="{{ $location->locationName }}">{{ $location->locationName }}</option>
                        @endforeach
                    </select>
                    @error('endLocation')
                    <small id="endLocation" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="routeName">Route Name</label>
                    <input type="text" id="routeName" class="form-control" name="routeName">
                    @error('routeName')
                    <small id="routeName" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="stoppage">Stoppage</label>
                    <select name="stoppage[]" id="stoppage" class="form-control" multiple="multiple">
                        <option value=""></option>
                    </select>
                    @error('stoppage')
                    <small id="stoppage" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="distance">Distance</label>
                    <div class="input-group">
                        <input type="text" name="distance" id="distance" class="form-control" placeholder="Distance">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">Kilometer</span>
                        </div>
                    </div>
                    @error('distance')
                    <small id="distance" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="approximateTime">Approximate Time</label>
                    <div class="input-group">
                        <input type="text" name="approximateTime" id="approximateTime" class="form-control" placeholder="Approximate Time">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">Hours</span>
                        </div>
                    </div>
                    @error('approximateTime')
                    <small id="approximateTime" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="">Select one</option>
                        <option value="1">Active</option>
                        <option value="0">Deactive</option>
                    </select>
                    @error('status')
                    <small id="status" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>


@endsection()


@section('custom_js')
<!-- seript here -->
<script>
    $(function() {
        $("#stoppage").select2();
    });

    function getCompanylocation(id) {
        let company_id = id;
        company_select = document.getElementById('alert_company');
        company_select.innerHTML = '';
        let location_name = `<option value=''>Select One</option>`;
        let location_value = '';
        if (company_id != null) {
            $.ajax({
                url: "{{ route('admin.get.company.route.info')}}",
                type: 'POST',
                dataType: 'json',
                data: {
                    _token: '{{ csrf_token() }}',
                    company_id: company_id
                },
                success: function(r) {
                    d = r.routes;
                    if (d) {
                        for (const i in d) {
                            location_name += `<option value='${d[i].id}'>${d[i].locationName}</option>`;
                            location_value += `<option value='${d[i].locationName}'>${d[i].locationName}</option>`;
                        }
                    } else {
                        document.getElementById('routemsg').innerHTML = "Please insert location first";
                    }
                    // document.getElementById("routeName").innerHTML = location_name;
                    document.getElementById("stoppage").innerHTML = location_value;
                    document.getElementById('startLocation').innerHTML = location_name;
                    document.getElementById('endLocation').innerHTML = location_name;
                    document.getElementById('routeName').value = '';
                    $("#stoppage").select2();

                }
            });

        } else {
            company_select.innerHTML = 'Select Company';
        }

        // const Http = new XMLHttpRequest();
        // const url = "{{ route('admin.get.company.route.info')}}"; 
        // Http.open('POST', url);
        // Http.send();
        // Http.onreadystatechange=(e) => {

        //     console.log( Http.responseText);
        // }
    }

    const startLocation = document.querySelector("#startLocation") ;
    startLocation.addEventListener('change',(e) => {
        // console.log(e.target.value);
        const startLocationTaxt  = e.target.options[e.target.selectedIndex].text;
        const addstartRoute = document.querySelector('#routeName');
        addstartRoute.value = `${startLocationTaxt} - `;
    });

    const endLocation = document.querySelector("#endLocation") ;
    endLocation.addEventListener('change',(e) => {
        // console.log(e.target.options[e.target.selectedIndex].text);
        const endLocationTaxt  = e.target.options[e.target.selectedIndex].text;
        const addstartRoute = document.querySelector('#routeName');
        addstartRoute.value += endLocationTaxt
    })
</script>

@endsection()