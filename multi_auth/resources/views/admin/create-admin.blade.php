@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection()

@section('main_content')

<h1 class="h3 mb-4 text-gray-800">{{ __('Add Company Admin') }}</h1>


<div class="card">
    <div class="card-body">
        @if( $msg = Session::get('success') )
        <div class="alert alert-primary" role="alert">
            {{ $msg }}
        </div>
        @endif

        <form method="POST" action="{{ route('admin.save') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="name">Name</label>
                    <input type="text" value="{{ old('name') }}" name="name" class="form-control" id="name" placeholder="Admin name">
                    @error('name')
                    <small id="name" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="email">Email</label>
                    <input type="email" value="{{ old('email') }}" name="email" class="form-control" id="email" placeholder="Admin email">
                    @error('email')
                    <small id="email" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="mobile">Mobile</label>
                    <input type="number" value="{{ old('mobile') }}" name="mobile" class="form-control" id="mobile" placeholder="Mobile no">
                    @error('mobile')
                    <small id="mobile" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="user_type_id">User type</label>
                    <select name="user_type_id" id="user_type_id" class="form-control">
                        <option value="">Select one</option>
                        @foreach( $usertypes as $usertype )
                        <option value="{{ $usertype->id }}">{{ $usertype->title}}</option>
                        @endforeach
                    </select>
                    @error('user_type_id')
                    <small id="user_type_id" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                @if( is_super_admin() ) 
                <div class="form-group col-md-6">
                    <label for="company_id">Company</label>
                    <select name="company_id" id="company_id" class="form-control">
                        <option value="">Select one</option>
                        @foreach( $companies as $com )
                        <option value="{{ $com->id }}">{{ $com->companyName }}</option>
                        @endforeach
                    </select>
                    @error('company_id')
                    <small id="company_id" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                @endif
            <div class="form-group col-md-6">
                <label for="password">Password</label>
                <input type="password" value="{{ old('password') }}" name="password" class="form-control" id="Password" placeholder="password no">
                @error('password')
                <small id="password" class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>
            <!-- <div class="form-group col-md-6">
                    <label for="image">Image</label>
                    <input type="file" name="image" class="form-control" id="image" onchange="proviewImage(this)">
                </div> -->
    </div>
    <div class="company-img">
        <img id="viewimage" src="{{asset('asset/img/company/avator.png')}}" alt="">
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>
</div>


@endsection()


@section('custom_js')
<!-- seript here -->

@endsection()