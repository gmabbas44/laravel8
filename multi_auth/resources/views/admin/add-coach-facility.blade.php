@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection()

@section('main_content')

<h1 class="h3 mb-4 text-gray-800">{{ __('Add New Coach Facelity') }}
    <a class="btn btn-primary float-right" href="{{ route('admin.coach.facelity.list') }}">Back</a>
</h1>

<div class="card">
    <div class="card-body">
    @if( $msg = Session::get('warning') )
        <div class="alert alert-danger" role="alert">
            {{ $msg }}
        </div>
    @endif

        <form method="POST" action="{{ route('admin.save.coach.facility')}}">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="title">Title</label>
                    <input type="text" value="{{ old('title') }}" name="title" class="form-control" id="title" placeholder="Coach title">
                    @error('title')
                    <small id="title" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="details">Details</label>
                    <input type="text" value="{{ old('details') }}" name="details" class="form-control" id="details" placeholder="Title details">
                    @error('details')
                    <small id="details" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                @if( is_super_admin() )
                <div class="form-group col-md-6">
                    <label for="company">Company</label>
                    <select name="company" id="company" class="form-control">
                        <option value="">Select one</option>
                        @foreach( $companies as $company)
                        <option value="{{ $company->id }}">{{ $company->companyName }}</option>
                        @endforeach
                    </select>
                    @error('company')
                    <small id="company" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                @endif
                <div class="form-group col-md-6">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="">Select one</option>
                        <option value="1">Active</option>
                        <option value="0">Deactive</option>
                    </select>
                    @error('status')
                    <small id="status" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>


@endsection()


@section('custom_js')
<!-- seript here -->

@endsection()