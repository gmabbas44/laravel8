@extends('admin.layouts.app')

@section('style')
<!-- slist here -->

@endsection

@section('main_content')
<h1 class="h3 mb-4 text-gray-800">{{ __('Bus List') }}
    <a class="btn btn-primary float-right" href="{{ route('admin.bus.reg')}}">Add New</a>
</h1>
@if( $msg = Session::get('success') )
<div class="alert alert-primary" role="alert">
    {{ $msg }}
</div>
@endif
<div class="card">

    <div class="card-body">
        <table class="table table-hover table-sm">
            <thead>
                <tr>
                    <th scope="col">Sl</th>
                    <th scope="col">Reg NO</th>
                    <th scope="col">Engine NO</th>
                    <th scope="col">Model NO</th>
                    <th scope="col">Chasis No</th>
                    <th scope="col">Total Seat</th>
                    <th scope="col">Flate type</th>
                    <th scope="col">Flate Facelity</th>
                    <th scope="col">Owner</th>
                    @if( is_super_admin() )
                    <th scope="col">Company Name</th>
                    @endif
                    <th scope="col">Ac Avilabel</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0; ?>
                @foreach( $buslist as $list )
                <?php $i++; ?>
                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $list->regNo }}</td>
                    <td>{{ $list->engineNo }}</td>
                    <td>{{ $list->modelNo }}</td>
                    <td>{{ $list->chasisNo }}</td>
                    <td>{{ $list->totalSeat }}</td>
                    <td>{{ $list->title }}</td>
                    <td>{{ $list->title }}</td>
                    <td>{{ $list->owner }}</td>
                    @if( is_super_admin() )
                    <td>{{ $list->companyName }}</td>
                    @endif
                    <td>
                        @if( $list->ac_available == 1 )
                        <span class="badge badge-pill badge-primary">Yes</span>
                        @else
                        <span class="badge badge-pill badge-danger">No</span>
                        @endif
                    </td>
                    <td>{{ status($list->status) }}</td>
                    <td>
                        <a class="text-primary" href="{{ route('admin.bus.edit', ['id' => $list->id ]) }}"><i class="fa fa-edit "></i></a>
                        ||
                        <a class="text-danger" id="delete-bus" data-toggle="modal" data-target="#deleteBus" url="{{ route('admin.bus.delete',['id' => $list->id])}}"><i class="fa fa-trash "></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>



<!-- temporary delete bus -->
<div class="modal fade" id="deleteBus" tabindex="-1" role="dialog" aria-labelledby="deleteBus" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Delete Bus Information?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Delete carefully, You are not retrieve Bus Information.</div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                <!-- <button class="btn btn-primary" type="submit">Delete</button> -->
                <a class="btn btn-primary text-white" id="bus-delete-btn">Delete</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<!-- seript here -->

<script>
    $(function() {
        $('#delete-bus').click(function() {
            let id = $(this).attr('url');
            $('#bus-delete-btn').attr({
                href: id
            });
        });
    });
</script>

@endsection