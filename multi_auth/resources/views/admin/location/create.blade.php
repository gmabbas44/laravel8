@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection()

@section('main_content')

<h1 class="h3 mb-4 text-gray-800">{{ __('Add Location') }}</h1>


<div class="card">
    <div class="card-body">
        @if( $msg = Session::get('warning') )
        <div class="alert alert-danger" role="alert">
            {{ $msg }}
        </div>
        @endif

        <form method="POST" action="{{ route('admin.location.save')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="locationName">Location Name</label>
                    <input list="locationNames" placeholder="Enter location name" class="form-control" name="locationName" id="locationName">
                    <datalist id="locationNames">
                        @foreach( $districts as $district)
                        <option value="{{ $district->name}}">
                        @endforeach
                    </datalist>
                    @error('locationName')
                    <small id="locationName" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="locationShortName">Location Short Name</label>
                    <input name="locationShortName" placeholder="Enter location name" class="form-control" id="locationShortName">
                    
                    @error('locationShortName')
                    <small id="locationShortName" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="description">Description</label>
                    <input type="text" value="{{ old('description') }}" name="description" class="form-control" id="description" placeholder="Enter Description">
                    @error('description')
                    <small id="description" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="googleMap">Google Map</label>
                    <input type="text" value="{{ old('googleMap') }}" name="googleMap" class="form-control" id="googleMap" placeholder="googleMap no">
                </div>
                @if( is_super_admin() )
                <div class="form-group col-md-6">
                    <label for="company">Company</label>
                    <select name="company" id="company" class="form-control">
                        <option value="">Select one</option>
                        @foreach( $companies as $company)
                        <option value="{{ $company->id}}">{{ $company->companyName}}</option>
                        @endforeach
                    </select>
                    @error('company')
                    <small id="company" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                @endif
                <div class="form-group col-md-6">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="">Select one</option>
                        <option value="1">Active</option>
                        <option value="0">Deactive</option>
                    </select>
                    @error('status')
                    <small id="status" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                
            </div>
            <div class="company-img">
                <img id="viewimage" src="{{asset('asset/img/company/avator.png')}}" alt="">
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>


@endsection()


@section('custom_js')
<!-- seript here -->

@endsection()