@extends('admin.layouts.app')

@section('style')
<!-- slocation here -->

@endsection

@section('main_content')
<h1 class="h3 mb-4 text-gray-800">{{ __('Trip location List') }}
<a class="btn btn-primary float-right" href="{{ route('admin.location.create')}}">Add New</a>
</h1>
@if( $msg = Session::get('success') )
<div class="alert alert-primary" role="alert">
    {{ $msg }}
</div>
@endif
<div class="card">
    
    <div class="card-body">
        <table class="table table-hover table-sm">
            <thead>
                <tr>
                    <th scope="col">Sl</th>
                    <th scope="col">Location Name</th>
                    <th scope="col">Location Short Name</th>
                    <th scope="col">Description</th>
                    @if( is_super_admin() )
                    <th scope="col">Company name</th>
                    @endif
                    <th scope="col">Is Deleted</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0 ; ?>
                @foreach( $locations as $location )
                <?php $i++ ; ?>
                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $location->locationName	 }}</td>
                    <td>{{ $location->locationShortName	 }}</td>
                    <td>{{ $location->description }}</td>
                    @if( is_super_admin() )
                    <td>{{ $location->companyName }}</td>
                    @endif
                    <td>{{ is_deleted($location->soft_delete) }}</td>
                    <td>{{ status($location->status) }}</td>
                    <td>
                        <a class="text-primary" href="{{ route('admin.location.edit', ['id' => $location->id] ) }}"><i class="fa fa-edit "></i></a>
                        ||
                        <a class="text-danger" onclick="deleteFun()" id="delete-driver" data-toggle="modal" data-target="#deletelocation" url="{{ route('admin.location.delete',['id' => $location->id])}}"><i class="fa fa-trash "></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


<!-- temporary delete bus -->
<div class="modal fade" id="deletelocation" tabindex="-1" role="dialog" aria-labelledby="deleteDriver" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Delete location Information?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Delete carefully, You are not retrieve Location.</div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary text-white" id="location-delete-btn">Delete</a>
            </div>
        </div>
    </div>
</div>


@endsection

@section('custom_js')
<!-- seript here -->

<script>
    function deleteFun() {
        let url = document.getElementById('delete-driver').getAttribute('url');
        document.getElementById('location-delete-btn').setAttribute('href', url);
    }
</script>

@endsection

