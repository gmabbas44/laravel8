@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection



@section('main_content')

<!-- main content -->
<h1 class="h3 mb-4 text-gray-800">Company List</h1>
@if( $msg = Session::get('success') )
<div class="alert alert-primary" role="alert">
    {{ $msg }}
</div>
@endif
<div class="card">
    <div class="card-body">
        <table class="table table-hover table-sm">
            <thead>
                <tr>
                    <th scope="col">Sl</th>
                    <th scope="col">Company Name</th>
                    <th scope="col">Company Email</th>
                    <th scope="col">Mobile</th>
                    <th scope="col">Image</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach( $companies as $c )
                <tr>
                    <th scope="row">{{ $c->id }}</th>
                    <td>{{ $c->companyName }}</td>
                    <td>{{ $c->companyEmail }}</td>
                    <td>{{ $c->mobile }}</td>
                    <td>{{ $c->image }}</td>
                    <td>{{ $c->status }}</td>
                    <td>
                        <a class="text-primary" href="{{ route('admin.edit.company', ['id' => $c->id])}}"><i class="fa fa-edit "></i></a>
                        ||
                        <a class="text-danger" id="delete-company" data-toggle="modal" data-target="#deleteCompany" url="{{ route('admin.delete.company',['id' => $c->id])}}"><i class="fa fa-trash "></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Logout Modal-->
<div class="modal fade" id="deleteCompany" tabindex="-1" role="dialog" aria-labelledby="deleteCompany" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Delete Company?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Delete carefully, You are not retrieve company.</div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                <!-- <button class="btn btn-primary" type="submit">Delete</button> -->
                <a class="btn btn-primary" id="company-delete-btn">Delete</a>
            </div>
        </div>
    </div>
</div>
@endsection


@section('custom_js')
<!-- seript here -->

<script>
    $( function() {
        $('#delete-company').click( function () {
            let id = $(this).attr('url');
            $('#company-delete-btn').attr({href:id});
        });
    });
</script>

@endsection