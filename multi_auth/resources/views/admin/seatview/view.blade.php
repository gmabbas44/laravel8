@extends('admin.layouts.app')

@section('style')
<!-- stype here -->
<style>
    .seat p {
        font-size: 14px;
        margin: 5px 0;
    }

    .seat-body {
        width: 40px;
        height: 40px;
        border-radius: 5px;
        padding: 8px;
        text-align: center;
        font-size: 16px;
        cursor: pointer;
    }

    .seat_selected_color {
        background-color: #f51b1b;
        color: #fff;
    }

    .seat_booked_color {
        background-color: #088c08;
        color: #fff;
    }

    .seat_confirm_color {
        background-color: #a92222;
        color: #fff;
    }

    .seat_empty_color {
        background-color: #f3f3f3;
        border: 1px solid #c1bfbf;
        color: #756d6d;
    }

    .seat_female_color {
        background-color: pink;
    }
</style>

@endsection



@section('main_content')

<?php

$alpht = range('A', 'Z');
$total_seat = $getData->totalSeat;
$column = explode('-', $getData->layout);
$countColumn = $column[0] + $column[1];

$mod = $total_seat % $countColumn;
$seat_row = ($total_seat - $mod) / $countColumn;


?>

<!-- main content -->

<h1 class="h3 mb-4 text-gray-800">{{ __('Trip Search list') }}
    <button class="btn btn-sm btn-primary float-right" onclick="refresh()"><i class="fas fa-redo"></i> Refresh</button>
</h1>



<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12 text-center">
                <h4>Company Name : {{ $getData->companyName}}</h4>
                <h6><span class="text-danger">Route name : </span> <span class="font-weight-bold">{{ $getData->routeName}}</span></h6>
                <p>Deptrue Time</p>
                <h6>Total Seat : {{ $getData->totalSeat}}</h6>
                <!-- <h6>Ticket Price : 400 </h6> -->
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-md-6">
                <div class="row ">
                    @foreach( $seatStatus as $key => $value )
                    <div class="col-md-2 col-sm-6">
                        <div class="seat">
                            <div class="seat-body {{ $key }}"></div>
                            <p>{{ $value }}</p>
                        </div>
                    </div>
                    @endforeach


                    <div class="mx-5 my-3">
                        <!-- seat row -1 -->

                        @for( $i = 0 ; $i < $seat_row ; $i +=1) <div class="row my-3 justify-content-md-center">
                            @for ( $j = 1 ; $j <= $countColumn ; $j +=1) <div class="col-2 m-1">
                                <div class="seat-body seat_empty_color chooseSeat" data-item="" seat_name='{{ $alpht[$i].$j}}'>{{ $alpht[$i].$j}}</div>
                    </div>
                    @if( $j == $column[0] )
                    <div class="col-2">

                    </div>
                    @endif
                    @endfor
                </div>
                @endfor

            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header">
                Passenger Information
            </div>
            <div class="card-body">
                <form>
                    <div class="form-group">
                        <label for="endPointOf">Droping Point</label>
                        <select class="form-control" id="endPointOf" onchange="dropingPoint(this.value)">
                            @foreach( $endPointOf as $endPoint )
                            <option>{{ $endPoint->locationName }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="passenger_moblie">Passenger Mobile NO</label>
                        <input type="number" name="" class="form-control" id="passenger_moblie" placeholder="Passenger Mobile">
                    </div>
                    <div class="form-group">
                        <label for="passenger_gender">Gender</label>
                        <div class="row mb-2">
                            <div class="col-md">
                                <div class="form-check">
                                    <input type="radio" name="gender" value="1" class="form-check-input" id="male">
                                    <label class="form-check-label" for="male">Ms</label>
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="form-check">
                                    <input type="radio" name="gender" value="2" class="form-check-input" id="female">
                                    <label class="form-check-label" for="female">Miss/Mrs</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="passenger_name">Passenger Name</label>
                        <input type="text" class="form-control" id="passenger_name" placeholder="Passenger Name">
                        <small id="passenger_name" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="passenger_email">Passenger Email NO</label>
                        <input type="email" class="form-control" id="passenger_email" placeholder="Passenger Email id">
                    </div>

                    <div class="form-group">
                        <label for="payment_status">Payment status</label>
                        <select class="form-control" id="payment_status">
                            <option>Select One</option>
                            <option>Paid</option>
                            <option>Advance</option>
                        </select>
                    </div>

                    <div class="form-group dis">
                        <label for="discount">Discount</label>
                        <div class="row mb-2">
                            <div class="col-md">
                                <div class="form-check">
                                    <input type="radio" name="discount" value="percentage" class="form-check-input discount-box" id="percentage">
                                    <label class="form-check-label discount" for="percentage">Percentage</label>
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="form-check">
                                    <input type="radio" name="discount" value="amount" class="form-check-input discount-box" id="amount">
                                    <label class="form-check-label" for="amount">Amount</label>
                                </div>
                            </div>
                        </div>
                        <input type="text" class="form-control d-none" id="discount" placeholder="Discount">
                    </div>
                    <hr>
                    <table class="table table-bordered text-right">
                        <tr>
                            <td width="30%">Seat</td>
                            <td id="seatPreview"></td>
                        </tr>
                        <tr>
                            <td>Per seat</td>
                            <td id="perSeat"></td>
                        </tr>
                        <tr>
                            <td>Cross Total</td>
                            <td id="crossTotal"></td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td id="grandTotalPriview"></td>
                        </tr>
                    </table>

                    <button type="submit" class="btn btn-primary">Continue</button>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection


@section('custom_js')
<!-- seript here -->
<script>
    let company_id = '{{ $getData->company_id }}';
    let trip_route_id = '{{ $getData->trip_route_id }}';
    const refresh = () => location.reload();

    let seatPreview = document.querySelector('#seatPreview');
    let perSeat = document.getElementById('perSeat');
    let crossTotal = document.getElementById('crossTotal');
    let grandTotalPriview = document.getElementById('grandTotalPriview');

    let countSeats = 0;

    $('body').on('click', '.chooseSeat', function() {

        let seat = $(this);
        if (seat.attr('data-item') !== 'selected') {
            seat.removeClass('seat_empty_color').addClass('seat_selected_color').attr('data-item', 'selected');
        } else {
            seat.removeClass('seat_selected_color').addClass('seat_empty_color').attr('data-item', '');
        }
        //reset seat serial for each click
        let seatSerial = "";

        $("div[data-item=selected]").each(function(i, x) {
            countSeats = i + 1;
            seatSerial += $(this).text().trim() + ", ";
        });
        let seatChoose = $(this).text().trim();

        seatPreview.textContent = seatSerial;
    });

    const dropingPoint = (name) => {
        const dropingPoint = name;
        $.ajax({
            dataType: 'json',
            url: "{{ route('get.ticket.price') }}",
            type: "POST",
            data: {
                dropingPoint: dropingPoint,
                company_id: company_id,
                trip_route_id: trip_route_id,
                _token: '{{ csrf_token() }}',
            },
            success: function(r) {
                perSeat.textContent = r.price;
                if (countSeats !== 0) {
                    const totalPrice = countSeats * Number.parseFloat(r.price);
                    crossTotal.textContent = totalPrice;
                    grandTotalPriview.textContent = totalPrice;
                } else {
                    alert('Please choose seat first');
                }

            }
        });
    }

    // const discount = document.querySelectorAll('.discount-box');

    // discount.addEventListener('click', (e) => {
    //     console.log(e.target.value);
    // });

    $('.discount-box').on('click', function() {
        document.getElementById('discount').classList.remove('d-none');

    });
</script>
@endsection