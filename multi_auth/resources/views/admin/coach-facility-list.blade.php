@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection

@section('main_content')
<h1 class="h3 mb-4 text-gray-800">{{ __('Coach Facility List') }}
<a class="btn btn-primary float-right" href="{{ route('admin.add.coach.facelity')}}">Add New</a>
</h1>
@if( $msg = Session::get('success') )
<div class="alert alert-primary" role="alert">
    {{ $msg }}
</div>
@endif
<div class="card">
    
    <div class="card-body">
        <table class="table table-hover table-sm">
            <thead>
                <tr>
                    <th scope="col">Sl</th>
                    <th scope="col">Title</th>
                    <th scope="col">Details</th>
                    @if( is_super_admin() )
                    <th scope="col">Company name</th>
                    @endif
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0 ; ?>
                @foreach( $coachfacilties as $type )
                <?php $i++ ; ?>
                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $type->title }}</td>
                    <td>{{ $type->details }}</td>
                    @if( is_super_admin() )
                    <td>{{ $type->companyName }}</td>
                    @endif
                    <td>{{ status($type->status) }}</td>
                    <td>
                        <a class="text-primary" href=""><i class="fa fa-edit "></i></a>
                        ||
                        <a class="text-danger" href=""><i class="fa fa-trash "></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


@endsection

@section('custom_js')
<!-- seript here -->

@endsection

