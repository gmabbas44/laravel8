@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection()

@section('main_content')

<h1 class="h3 mb-4 text-gray-800">{{ __('Registration new Bus') }}
    <a class="btn btn-primary float-right" href="{{ route('admin.bus.list') }}">Back</a>
</h1>

<div class="card">
    <div class="card-body">
        @if( $msg = Session::get('warning') )
        <div class="alert alert-danger" role="alert">
            {{ $msg }}
        </div>
        @endif

        <form method="POST" action="{{ route('admin.bus.add')}}">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="regNo">Registration</label>
                    <input type="text" value="{{ old('regNo') }}" name="regNo" class="form-control" id="regNo" placeholder="Enter Bus Registration">
                    @error('regNo')
                    <small id="regNo" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="engineNo">Engine No</label>
                    <input type="text" value="{{ old('engineNo') }}" name="engineNo" class="form-control" id="engineNo" placeholder="Enter Bus Engine NO">
                    @error('engineNo')
                    <small id="engineNo" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="modelNo">Model No</label>
                    <input type="text" value="{{ old('modelNo') }}" name="modelNo" class="form-control" id="modelNo" placeholder="Enter Bus Model No">
                    @error('modelNo')
                    <small id="modelNo" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-3">
                    <label for="chasisNo">Chasis No</label>
                    <input type="text" value="{{ old('chasisNo') }}" name="chasisNo" class="form-control" id="chasisNo" placeholder="Enter Bus Chasis No">
                    @error('chasisNo')
                    <small id="chasisNo" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-3">
                    <label for="layout">Bus Layout</label>
                    <select class="form-control" name="layout" id="layout">
                        <option value="">Select One</option>

                    </select>
                    @error('layout')
                    <small id="layout" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-3">
                    <label for="totalSeat">Total Seat</label>
                    <input type="number" onkeyup="myFun( this.value )" min="10" value="{{ old('totalSeat') }}" name="totalSeat" class="form-control" id="totalSeat" placeholder="Enter Bus Chasis No">
                    @error('totalSeat')
                    <small id="totalSeat" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-3">
                    <label for="lastSeat">Last Seat Availabe</label>
                    <select class="form-control" name="lastSeat" id="lastSeat">
                        <option value="1">Yes</option>
                        <option value="0">NO</option>
                    </select>
                    @error('lastSeat')
                    <small id="lastSeat" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="from-group col-md-6">
                    <label for="seatNumbers">Seat Number</label>
                    <textarea class="form-control" name="seatNumbers" id="seatNumbers" cols="30" rows="3" placeholder="Enter seat number"></textarea>
                    <small id="lastSeat" class="form-text text-danger">*Use comma to separate the seat number, like A1, A2,</small>
                    @error('seatNumbers')
                    <small id="seatNumbers" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                <label for="">Seat Layout</label>
                    <div id="show_seat">

                    </div>
                </div>

                @if( is_super_admin() )
                <div class="form-group col-md-4">
                    <label for="company">Company</label>
                    <select class="form-control" name="company" id="company" onchange="loadCompany(this.value)">
                        <option value="">Select Company</option>
                        @foreach( $companies as $c )
                        <option value="{{ $c->id }}">{{ $c->companyName }}</option>
                        @endforeach
                    </select>
                    <small id="alert_company" class="form-text text-danger"></small>
                    @error('company')
                    <small id="company" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                @endif


                <div class="form-group col-md-4">
                    <label for="coach_type">Coach Type</label>
                    <select class="form-control" name="coach_type" id="coach_type">
                        <option value="">Select Coach type</option>
                        @if( $coach_types )
                        @foreach( $coach_types as $type )
                        <option value="{{ $type->id }}">{{ $type->title}}</option>
                        @endforeach
                        @endif
                    </select>
                    @error('coach_type')
                    <small id="coach_type" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="coach_facility">Coach Facility</label>
                    <select class="form-control" name="coach_facility" id="coach_facility">
                        <option value="">Select Coach facility</option>
                        @if( $coach_facilities)
                        @foreach( $coach_facilities as $cf )
                        <option value="{{ $cf->id }}">{{ $cf->title }}</option>
                        @endforeach
                        @endif
                    </select>
                    @error('coach_facility')
                    <small id="coach_facility" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>


                <div class="form-group col-md-4">
                    <label for="owner">Owner</label>
                    <input type="text" class="form-control" name="owner" id="owner" value="{{ old('owner') }}" placeholder="Enter Bus owner Name">
                    @error('owner')
                    <small id="owner" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="ac_available">AC Availabe</label>
                    <select class="form-control" name="ac_available" id="ac_available">
                        <option value="1">Yes</option>
                        <option value="0">NO</option>
                    </select>
                    @error('ac_available')
                    <small id="ac_available" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-md-4">
                    <label for="status">Status</label>
                    <select class="form-control" name="status" id="status">
                        <option value="">Select One</option>
                        <option value="1">Yes</option>
                        <option value="0">NO</option>
                    </select>
                    @error('status')
                    <small id="status" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>

<!-- =========================== -->
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="seat_show" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- =========================== -->

@endsection()


@section('custom_js')
<!-- seript here -->
<script>
    /* ============ for layout view =========== */
    let layouts = ['1-1', '1-2', '2-1', '2-2', '2-3', '3-2'];
    let layforview = '<option value="">Select One</option>';

    for (i in layouts) {
        layforview += `<option value="${layouts[i]}">${layouts[i]}</option>`;
    }
    document.getElementById('layout').innerHTML = layforview;

    // for get company data with ajax request
    function loadCompany(id) {

        let company_id = id,
            i, j, coach_type = '',
            coach_facility = '';

        document.getElementById('alert_company').textContent = '';

        if (company_id !== null) {
            $.ajax({
                dataType: 'json',
                url: "{{ route('admin.get.company.facility') }}",
                type: "POST",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: company_id
                },
                success: function(r) {

                    const ct = r.ct;
                    const cf = r.cf;

                    // for( const i in  r ) {
                    //     coach_type += `<option value='${r.ct[i].id}'>${r.ct[i].title}</option>`;
                    //     coach_facility += `<option value='${r.cf[i].id}'>${r.cf[i].title}</option>`;
                    // }

                    for (const i in ct) {
                        coach_type += `<option value='${ct[i].id}'>${ct[i].title}</option>`;
                    }
                    for (const j in cf) {
                        coach_facility += `<option value='${cf[j].id}'>${cf[j].title}</option>`;
                    }
                    document.getElementById('coach_type').innerHTML = coach_type;
                    document.getElementById('coach_facility').innerHTML = coach_facility;
                    // console.log(coach_type);
                }
            });
        } else {
            document.getElementById('alert_company').textContent = 'Select Company';
        }
    }

    function myFun(seatno) {
        // get inseat layout information
        let totalSeat, layout, countColumn, seatColumn, seatRow, MOD;
        totalSeat = seatno;
        layout = document.getElementById('layout').value;
        // console.log(layout);

        // process data 
        countColumn = layout.split('-');
        seatColumn = parseInt(countColumn[0]) + parseInt(countColumn[1]);
        MOD = totalSeat % seatColumn;
        seatRow = (totalSeat - MOD) / seatColumn;
        // console.log('row : ' + seatRow);
        // console.log('column : ' + seatColumn);

        // for alphabet generator
        let alphabetList = [],
            alphabet, upperAlpha;
        for (i = 0; i < 26; i += 1) {
            alphabet = (i + 10).toString(36);
            upperAlpha = alphabet.toUpperCase();
            alphabetList.push(upperAlpha);
        }
        // console.log(alphabetList);

        let contain = '<div class="card"> <div class="card-body">',
            seatNumbers = '';
        for (i = 0; i < seatRow; i += 1) {
            contain += '<div class="row my-2">';
            // console.log(seatRow);
            for (j = 1; j <= seatColumn; j += 1) {
                contain += `<div class="col-md"><a class="btn btn-primary btn-sm text-white seatname">${alphabetList[i] + [j]}</a></div>`;
                if (j == countColumn[0]) {
                    contain += `<div class="col-md"><a class="btn btn-sm px-2"></a></div>`;
                }
                seatNumbers += alphabetList[i] + [j] + ', ';
                // console.log(alphabetList[i] + [j] + ', ');
            }
            contain += '</div>';
        }
        contain += '</div></div>';
        // $('#seat_show').modal('show');
        document.getElementById('seatNumbers').textContent = seatNumbers;
        document.getElementById('seatNumbers').style.color = '#4e73df';
        document.getElementById('show_seat').innerHTML = contain;
     
    }

    $('body').on('click', '.seatname', function(){
        let seatname = $(this).text();
        alert(seatname);
    })

</script>
@endsection()