@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection()

@section('main_content')

<h1 class="h3 mb-4 text-gray-800">Add New Company</h1>


<div class="card">
    <div class="card-body">
    @if( $msg = Session::get('success') )
        <div class="alert alert-primary" role="alert">
            {{ $msg }}
        </div>
    @endif

        <form method="POST" action="{{ route('admin.save.company')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="companyName">Company name</label>
                    <input type="text" value="{{ old('companyName') }}" name="companyName" class="form-control" id="companyName" placeholder="Company name">
                    @error('companyName')
                    <small id="companyName" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="companyEmail">Email</label>
                    <input type="email" value="{{ old('companyEmail') }}" name="companyEmail" class="form-control" id="companyEmail" placeholder="Company email">
                    @error('companyEmail')
                    <small id="companyEmail" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="mobile">Mobile</label>
                    <input type="number" value="{{ old('mobile') }}" name="mobile" class="form-control" id="mobile" placeholder="Mobile no">
                    @error('mobile')
                    <small id="mobile" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="">Select one</option>
                        <option value="1">Active</option>
                        <option value="0">Deactive</option>
                    </select>
                    @error('status')
                    <small id="status" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <!-- <div class="form-group col-md-6">
                    <label for="image">Image</label>
                    <input type="file" name="image" class="form-control" id="image" onchange="proviewImage(this)">
                </div> -->
            </div>
            <div class="company-img">
                <img id="viewimage" src="{{asset('asset/img/company/avator.png')}}" alt="">
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>


@endsection()


@section('custom_js')
<!-- seript here -->

@endsection()