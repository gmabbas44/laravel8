@extends('admin.layouts.app')

@section('style')
<!-- stype here -->

@endsection



@section('main_content')

<!-- main content -->
<h1 class="h3 mb-4 text-gray-800">{{ __('User List') }}</h1>
@if( $msg = Session::get('success') )
<div class="alert alert-primary" role="alert">
    {{ $msg }}
</div>
@endif

<div class="card">
    <div class="card-body">
        <table class="table table-hover table-sm">
            <thead>
                <tr>
                    <th scope="col">Sl</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Mobile</th>
                    <th scope="col">Comany name</th>
                    <th scope="col">Access Title</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach( $all as $user )
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->mobile }}</td>
                    <td>
                        @if( $user->company_id == 0)
                        {{ __('This is Super Admin') }}
                        @else
                        {{ $user->companyName }}
                        @endif
                    </td>
                    <td>{{ $user->accesstitle }}</td>
                    <td>
                        <a class="text-primary" href=""><i class="fa fa-edit "></i></a>
                        ||
                        <a class="text-danger" href=""><i class="fa fa-trash "></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection


@section('custom_js')
<!-- seript here -->

@endsection