<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketPrice extends Model
{
    use HasFactory;
    
    public function getIdAttribute( $value )
    {
        return encryptIt( $value );
    }

    public static function getAll()
    {
        return self::select('ticket_prices.id', 'companies.companyName', 'trip_routes.routeName', 'coach_types.title', 'ticket_prices.endPointOf', 'ticket_prices.price', 'ticket_prices.soft_delete')
                    ->join('companies', 'companies.id', '=', 'ticket_prices.company_id')
                    ->join('trip_routes', 'trip_routes.id', '=', 'ticket_prices.trip_route_id')
                    ->join('coach_types', 'coach_types.id', '=', 'ticket_prices.coach_type_id')
                    ->get();
    }
}
