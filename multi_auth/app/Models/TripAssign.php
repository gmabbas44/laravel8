<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Prophecy\Doubler\Generator\Node\ReturnTypeNode;

class TripAssign extends Model
{
    use HasFactory;

    public function getIdAttribute( $value )
    {
        return encryptIt($value);
    }
    // public function setIdAttribute( $value ) {
    //     return decryptIt($value);
    // }

    public static function getAll()
    {
        return self::select('trip_assigns.id', 'trip_assigns.id_no', 'trip_assigns.coachNo', 'trip_routes.routeName', 'companies.companyName', 'trip_assigns.startDate', 'trip_assigns.endDate', 'trip_assigns.status', 'trip_assigns.soft_delete')
            ->join('companies', 'companies.id', '=', 'trip_assigns.company_id')
            ->join('trip_routes', 'trip_routes.id', '=', 'trip_assigns.trip_route_id')
            ->where('trip_assigns.status', '=', 1)
            ->get();
    }

    public static function companyTripAssign($company_id)
    {
        return self::select('trip_assigns.id', 'trip_assigns.id_no', 'trip_assigns.coachNo', 'trip_routes.routeName', 'companies.companyName', 'trip_assigns.startDate', 'trip_assigns.endDate', 'trip_assigns.status', 'trip_assigns.soft_delete')
            ->join('companies', 'companies.id', '=', 'trip_assigns.company_id')
            ->join('trip_routes', 'trip_routes.id', '=', 'trip_assigns.trip_route_id')
            ->where('trip_assigns.status', '=', 1)
            ->where('trip_assigns.company_id', '=', $company_id)
            ->get();
    }

    public static function getAllAssign()
    {
        return self::select('trip_assigns.id', 'trip_routes.routeName', 'companies.companyName', 'trip_assigns.startPoint', 'trip_assigns.endPoint', 'trip_assigns.startDate', 'trip_assigns.endDate', 'trip_routes.distance', 'trip_routes.approximateTime', 'bus_regs.totalSeat', 'ticket_prices.price')
        ->join('companies', 'companies.id', '=', 'trip_assigns.company_id')
        ->join('trip_routes', 'trip_routes.id', '=', 'trip_assigns.trip_route_id')
        ->join('bus_regs', 'bus_regs.id', '=', 'trip_assigns.bus_reg_id')
        ->join('ticket_prices', 'ticket_prices.trip_route_id', '=', 'trip_assigns.trip_route_id')
        ->get();
    }
    
}
