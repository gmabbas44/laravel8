<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    use HasFactory;
    // protected $table = 'drivers as dr';

    public function getIdAttribute( $value )
    {
        return encryptIt( $value );
    }
    public static function getAll()
    {
        return self::select('drivers.id', 'drivers.driverName', 'drivers.driverPhone', 'drivers.nidNo', 'drivers.drivingLicenseNo', 'drivers.joinDate', 'drivers.leaveDate', 'companies.companyName', 'drivers.status', 'drivers.soft_delete')
                    ->join('companies', 'companies.id', '=', 'drivers.company_id')
                    ->where('drivers.status', '=', 1)
                    ->get();
    }
}
