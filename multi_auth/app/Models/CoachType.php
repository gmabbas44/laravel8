<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CoachType extends Model
{
    use HasFactory;

    public function getAll()
    {
        return $this->select('coach_types.id', 'coach_types.title', 'coach_types.details', 'companies.companyName', 'coach_types.status')
                    ->join('companies', 'coach_types.company_id', '=', 'companies.id')
                    ->get();
    }
    public function thisCoachType( $company_id )
    {
        return $this->select('coach_types.id', 'coach_types.title', 'coach_types.details', 'companies.companyName', 'coach_types.status')
                    ->join('companies', 'coach_types.company_id', '=', 'companies.id')
                    ->where('company_id', '=', $company_id)
                    ->get();
    }
    public static function getinfoByCompanyId( $company_id )
    {
        return self::select('id', 'title')->where('company_id', '=',$company_id)->where('status', '=', 1)->get();
    }
}
