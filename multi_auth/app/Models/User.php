<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'mobile',
        'password',
        'user_type_id',
        'company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function allUsers()
    {
        // return $this->hasMany('UserType');
        return $this->select('users.id', 'users.name', 'users.email', 'users.mobile', 'users.company_id', 'companies.companyName', 'user_types.title as accesstitle')
                    ->join('user_types', 'users.user_type_id', '=', 'user_types.id')
                    ->leftjoin('companies', 'users.company_id', '=', 'companies.id')
                    ->get();
    }

    public function companyAdmin( $company_id)
    {
        return $this->select('users.id', 'users.name', 'users.email', 'users.mobile', 'users.company_id', 'companies.companyName', 'user_types.title as accesstitle')
                    ->join('user_types', 'users.user_type_id', '=', 'user_types.id')
                    ->leftjoin('companies', 'users.company_id', '=', 'companies.id')
                    ->where('company_id', '=', $company_id)
                    ->get();
    }
}
