<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusReg extends Model
{
    use HasFactory;

    public function getIdAttribute( $value )
    {
        return encryptIt( $value );
    }
    public function getAll()
    {
        return $this->select('bus_regs.id', 'bus_regs.regNo', 'bus_regs.engineNo', 'bus_regs.modelNo', 'bus_regs.chasisNo', 'bus_regs.totalSeat', 'coach_types.title', 'coach_facilities.title', 'bus_regs.owner', 'companies.companyName', 'bus_regs.ac_available', 'bus_regs.status')
        ->join('coach_types', 'coach_types.id', '=', 'bus_regs.coach_type_id')
        ->join('coach_facilities', 'coach_facilities.id', '=', 'bus_regs.coach_facility_id')
        ->join('companies', 'companies.id', '=', 'bus_regs.company_id')
        ->where('bus_regs.soft_delete', '=', 0 )
        ->orderBy('bus_regs.id', 'DESC')
        ->get();
    }
    public function getComopanyBus( $id )
    {
        return $this->select('bus_regs.id', 'bus_regs.regNo', 'bus_regs.engineNo', 'bus_regs.modelNo', 'bus_regs.chasisNo', 'bus_regs.totalSeat', 'coach_types.title', 'coach_facilities.title', 'bus_regs.owner', 'companies.companyName', 'bus_regs.ac_available', 'bus_regs.status')
        ->join('coach_types', 'coach_types.id', '=', 'bus_regs.coach_type_id')
        ->join('coach_facilities', 'coach_facilities.id', '=', 'bus_regs.coach_facility_id')
        ->join('companies', 'companies.id', '=', 'bus_regs.company_id')
        ->where('coach_types.company_id', '=', $id )
        ->where('coach_facilities.company_id', '=', $id )
        ->where('bus_regs.company_id', '=', $id)
        ->where('bus_regs.soft_delete', '=', 0 )
        ->orderBy('bus_regs.id', 'DESC')
        ->get();
    }
    
}
