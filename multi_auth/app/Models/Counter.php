<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Counter extends Model
{
    use HasFactory;

    function getIdAttribute($value)
    {
        return encryptIt($value);
    }
    public static function getAll()
    {
        return self::select( 'counters.id','counters.counterName', 'companies.companyName', 'locations.locationName', 'counters.address', 'counters.status', 'counters.soft_delete')
                ->join('companies', 'companies.id', '=', 'counters.company_id')
                ->join('locations', 'locations.id', '=', 'counters.location_id')
                ->get();
    }
}
