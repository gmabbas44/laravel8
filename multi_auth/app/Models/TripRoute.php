<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TripRoute extends Model
{
    use HasFactory;

    public function getIdAttribute( $id )
    {
        return encryptIt( $id );
    }
    public static function getAll()
    {
        return self::select('trip_routes.id', 'trip_routes.routeName', 'trip_routes.startLocationName', 'trip_routes.endLocationName', 'trip_routes.stoppage', 'trip_routes.distance', 'trip_routes.approximateTime', 'companies.companyName', 'trip_routes.status', 'trip_routes.soft_delete')
                    ->join('companies', 'companies.id', '=', 'trip_routes.company_id')
                    ->get();
    }
}
