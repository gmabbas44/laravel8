<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coachfacility extends Model
{
    use HasFactory;
    protected $table = 'coach_facilities';

    public function getAll()
    {
        return $this->select('coach_facilities.id', 'coach_facilities.title', 'coach_facilities.details', 'companies.companyName', 'coach_facilities.status')
                    ->join('companies', 'coach_facilities.company_id', '=', 'companies.id')
                    ->get();
    }
    public function thisCoachFacelity( $company_id )
    {
        return $this->select('coach_facilities.id', 'coach_facilities.title', 'coach_facilities.details', 'companies.companyName', 'coach_facilities.status')
                    ->join('companies', 'coach_facilities.company_id', '=', 'companies.id')
                    ->where('company_id', '=', $company_id)
                    ->get();
    }
    public static function getInfoByCompanyId( $company_id ) {
        return self::select('id', 'title')->where('status', '=', 1)->where('company_id', '=', $company_id )->get();
    }
}
