<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    public function getIdAttribute( $value )
    {
        return encryptIt($value);
    }

    public function getAll()
    {
        return $this->select('locations.id', 'locations.locationName', 'locations.locationShortName', 'locations.description', 'companies.companyName', 'locations.soft_delete', 'locations.status')
                    ->join('companies', 'companies.id', '=', 'locations.company_id')
                    ->orderBy('id', 'DESC')
                    ->get();
    }

}
