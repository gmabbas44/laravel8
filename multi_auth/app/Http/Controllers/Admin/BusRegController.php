<?php

namespace App\Http\Controllers\Admin;

use App\Models\BusReg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Coachfacility;
use App\Models\CoachType;
use App\Models\Company;
use App\Models\TripRoute;
use Illuminate\Support\Facades\Auth;

class BusRegController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(BusReg $busReg)
    {
        if (is_super_admin()) {
            $buslist = $busReg->getAll();
        } else {
            $buslist = $busReg->getComopanyBus(Auth::user()->company_id);
        }
        return view('admin.bus-list', compact('buslist'));
    }

    public function create()
    {
        if (is_super_admin()) {
            $companies = Company::select('id', 'companyName')->get();
            $coach_types = [];
            $coach_facilities = [];
        } else {
            $companies = [];
            $coach_types =  CoachType::getinfoByCompanyId(Auth::user()->company_id);
            $coach_facilities = Coachfacility::getinfoByCompanyId(Auth::user()->company_id);
        }

        return view('admin.bus-reg', compact('companies', 'coach_types', 'coach_facilities'));
    }

    public function save(Request $request, BusReg $busReg)
    {
        if (is_super_admin()) {
            $request->validate([
                'regNo'         => 'required',
                'engineNo'      => 'required',
                'modelNo'       => 'required',
                'chasisNo'      => 'required',
                'layout'        => 'required',
                'totalSeat'     => 'required',
                'lastSeat'      => 'required',
                'seatNumbers'   => 'required',
                'coach_type'    => 'required',
                'coach_facility' => 'required',
                'company'       => 'required',
                'owner'         => 'required',
                'ac_available'  => 'required',
                'status'        => 'required'
            ]);
        } else {
            $request->validate([
                'regNo'         => 'required',
                'engineNo'      => 'required',
                'modelNo'       => 'required',
                'chasisNo'      => 'required',
                'layout'        => 'required',
                'totalSeat'     => 'required',
                'lastSeat'      => 'required',
                'seatNumbers'   => 'required',
                'coach_type'    => 'required',
                'coach_facility' => 'required',
                'owner'         => 'required',
                'ac_available'  => 'required',
                'status'        => 'required'
            ]);
        }

        $busReg->regNo = $request->input('regNo');
        $busReg->engineNo = $request->input('engineNo');
        $busReg->modelNo = $request->input('modelNo');
        $busReg->chasisNo = $request->input('chasisNo');
        $busReg->layout = $request->input('layout');
        $busReg->totalSeat = $request->input('totalSeat');
        $busReg->lastSeat = $request->input('lastSeat');
        $busReg->seatNumbers = $request->input('seatNumbers');
        $busReg->coach_type_id = $request->input('coach_type');
        $busReg->coach_facility_id = $request->input('coach_facility');
        $busReg->company_id = is_super_admin() ? $request->input('company') : Auth::user()->company_id;
        $busReg->owner = $request->input('owner');
        $busReg->ac_available = $request->input('ac_available');
        $busReg->status = $request->input('status');
        $result =  $busReg->save();
        if ($result) {
            return redirect()->route('admin.bus.list')->with('success', 'Bus register Successfully!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    }
    public function store(Request $request)
    {
        //
    }

    public function show(BusReg $busReg)
    {
        //
    }

    public function edit(BusReg $busReg, $id)
    {
        $id = decryptIt($id);
        $getbusinfo = $busReg->find($id);
        if ( is_super_admin() ) {
            $companies = Company::select('id', 'companyName')->get();
            $coach_types = CoachType::getinfoByCompanyId( $getbusinfo->company_id );
            $coach_facilities = Coachfacility::getinfoByCompanyId( $getbusinfo->company_id );

        } else {
            $companies = [];
            $coach_types = CoachType::getinfoByCompanyId( Auth::user()->company_id );
            $coach_facilities = Coachfacility::getinfoByCompanyId( Auth::user()->company_id );
        }
        $layouts = ['1-1', '1-2', '2-1', '2-2', '2-3', '3-2'];

        return view('admin.edit-bus', compact('getbusinfo', 'companies', 'coach_types', 'coach_facilities', 'layouts'));
    }

    public function update(Request $request, BusReg $busReg)
    {
        $id = decryptIt( $request->id );
        $update = $busReg->find( $id );

        $update->regNo = $request->regNo;
        $update->engineNo = $request->engineNo;
        $update->modelNo = $request->modelNo;
        $update->chasisNo = $request->chasisNo;
        $update->layout = $request->layout;
        $update->totalSeat = $request->totalSeat;
        $update->lastSeat = $request->lastSeat;
        $update->seatNumbers = $request->seatNumbers;
        $update->coach_type_id = $request->coach_type;
        $update->coach_facility_id = $request->coach_facility;
        $update->company_id = is_super_admin() ? $request->company : Auth::user()->company_id;
        $update->owner = $request->owner;
        $update->ac_available = $request->ac_available;
        $update->status = $request->status;
        $result =  $update->save();

        if ($result) {
            return redirect()->route('admin.bus.list')->with('success', 'Bus Information Update Successfully!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
        
    }

    public function delete(BusReg $busReg, $id) 
    {
        $id = decryptIt( $id );

        $softDelete = $busReg->find( $id );
        $softDelete->soft_delete = 1;
        $result = $softDelete->save();
        if ($result) {
            return redirect()->route('admin.bus.list')->with('success', 'Bus Information Delete Successfully!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    }

    public function destroy(BusReg $busReg)
    {
        //
    }
    public function getCoachtypeAndfacility(Request $request)
    {
        $company_id = $request->input('id');
        $data['ct'] = CoachType::getinfoByCompanyId( $company_id );
        $data['cf'] = Coachfacility::getinfoByCompanyId( $company_id );
        return response()->json($data);
    }

    public function companyBusInfo(Request $request)
    {
        // $company_id = decryptIt($request->input('company_id'));
        $company_id = $request->input('company_id');

        $result['businfo'] = BusReg::select('id', 'regNo')
                                    ->where('company_id', '=', $company_id)
                                    ->where('soft_delete', '=', 0)
                                    ->get();
        $result['routeinfo'] = TripRoute::select('id', 'routeName')
                                        ->where('company_id', '=', $company_id)
                                        ->where('soft_delete', '=', 0)
                                        ->get();
        
         return response()->json($result);

    }


    
}
