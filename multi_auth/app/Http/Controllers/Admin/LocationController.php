<?php

namespace App\Http\Controllers\Admin;

use App\Models\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index( Location $location )
    {
        if ( is_super_admin() ) {
            $locations = $location->getAll();
        } else {
            $locations = $location->where('company_id', '=', Auth::user()->company_id)->where('soft_delete', '=', 0)->orderBy('id', 'DESC')->get();
        }
        
        return view('admin.location.list', compact('locations'));
    }
    public function create( )
    {
        $districts = DB::table('districts')->select('name')->get();
        $companies = Company::select('id', 'companyName')->get();
        return view('admin.location.create', compact('companies', 'districts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request, Location $location)
    {
        if ( is_super_admin() ) {
            $request->validate([
                'locationName'      => 'required',
                'locationShortName' => 'required',
                'description'       => 'required',
                'company'           => 'required',
                'status'            => 'required'
            ]);
        } else {
            $request->validate([
                'locationName'      => 'required',
                'locationShortName' => 'required',
                'description'       => 'required',
                'status'            => 'required'
            ]);
        }
        $location->locationName         = $request->input('locationName');
        $location->locationShortName    = $request->input('locationShortName');
        $location->description          = $request->input('description');
        $location->googleMap            = $request->input('googleMap');
        $location->company_id           = is_super_admin() ? $request->input('company') : Auth::user()->company_id ;
        $location->status               = $request->input('status');

        $res = $location->save();
        if ($res) {
            return redirect()->route('admin.location.list')->with('success', 'Location Successfully Added!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        //
    }

    public function edit(Location $location, $id)
    {
        $location_id = decryptIt( $id );
        $getlocation = $location->find( $location_id );
        $districts = DB::table('districts')->select('name')->get();
        $companies = Company::select('id', 'companyName')->get();
        return view('admin.location.edit', compact('getlocation', 'districts', 'companies'));
    }

    public function update(Request $request, Location $location)
    {
        $location_id = decryptIt( $request->id );
        $update = $location->find( $location_id );
        $update->locationName       = $request->locationName;
        $update->locationShortName  = $request->locationShortName;
        $update->description        = $request->description;
        $update->googleMap          = $request->googleMap ;
        $update->company_id         = is_super_admin() ? $request->company : Auth::user()->company_id ; ;
        $update->status = $request->status;

        $res = $update->save();
        if ($res) {
            return redirect()->route('admin.location.list')->with('success', 'Location update Successfully!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    }
    public function delete( $id )
    {
        $id = decryptIt( $id );

        $softDelete = Location::find( $id );
        $softDelete->soft_delete = 1;
        $result = $softDelete->save();
        if ($result) {
            return redirect()->route('admin.location.list')->with('success', 'Location Delete Successfully!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        //
    }
}
