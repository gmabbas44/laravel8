<?php

namespace App\Http\Controllers\Admin;

use App\Models\TripAssign;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BusReg;
use App\Models\Company;
use App\Models\TripRoute;
use Illuminate\Support\Facades\Auth;

class TripAssignController extends Controller
{
    public $company_id ;

    public function __construct()
    {
        $this->middleware('auth');
        // $this->company_id = Auth::user()->company_id;
    }

    public function index()
    {
        $TripAssign =  TripAssign::getAll();
        is_super_admin() ? $allTripAssign = $TripAssign : $allTripAssign = TripAssign::companyTripAssign(Auth::user()->company_id);
        return view('admin.tirpassign.list', compact('allTripAssign'));
    }

    public function create()
    {
        $companies = Company::select('id', 'companyName')->get();
        $busInfo = BusReg::select('id', 'regNo')->where('soft_delete', '=', 0)->where('status', '=', 1)->where('company_id', '=', Auth::user()->company_id)->get();
        $routeInfo = TripRoute::select('id', 'routeName')->where('company_id', '=', Auth::user()->company_id)->where('soft_delete', '=', 0)->get();
        if ( is_super_admin() ) {
            $companies ;
            $busInfo = [];
            $routeInfo = [];
        }else {
            $companies = [];
            $busInfo ;
            $routeInfo ;
        }
    
        return view('admin.tirpassign.create', compact('companies', 'busInfo', 'routeInfo'));
    }

    public function store(Request $request, TripAssign $tripAssign)
    {
        $request->validate([
            'bus_reg_id'    => 'required',
            'trip_route_id' => 'required',
            'startPoint'    => 'required',
            'endPoint'      => 'required',
            'startDate'     => 'required',
            'endDate'       => 'required',
            'status'        => 'required',
        ]);

        
        $tripAssign->trip_route_id	 = decryptIt($request->input('trip_route_id'));
        $tripAssign->bus_reg_id = decryptIt($request->input('bus_reg_id'));

        $routeName = TripRoute::select('routeName')->where('id', '=', decryptIt($request->input('trip_route_id')))->first();
        $busRegNo = BusReg::select('regNo')->where('id', '=', decryptIt($request->input('bus_reg_id')))->first();
        
        $coachNo = $busRegNo->regNo . '-' . $routeName->routeName;
       
        $tripAssign->id_no = time(); 
        $tripAssign->startPoint = $request->input('startPoint');
        $tripAssign->endPoint = $request->input('endPoint');
        $tripAssign->company_id = is_super_admin() ? $request->input('company_id') : Auth::user()->company_id;
        $tripAssign->coachNo = $coachNo ;
        $tripAssign->startDate = $request->input('startDate');
        $tripAssign->endDate = $request->input('endDate');
        $tripAssign->status = $request->input('status');
        $result = $tripAssign->save();

        if ($result) {
            return redirect()->route('trip-assign.index')->with('success', 'Trip Assign Added Successfully!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TripAssign  $tripAssign
     * @return \Illuminate\Http\Response
     */
    public function show(TripAssign $tripAssign)
    {
        //
    }

    public function edit( $id, TripAssign $tripAssign )
    {
        $tripAssignId = decryptIt( $id );
        $tps =  $tripAssign->find($tripAssignId)->first();
        $busInfo = BusReg::select('id', 'regNo')->where('soft_delete', '=', 0)->where('status', '=', 1)->where('company_id', '=', $tps->company_id)->get();
        $routeInfo = TripRoute::select('id', 'routeName')->where('company_id', '=', $tps->company_id)->where('soft_delete', '=', 0)->get();

        return view('admin.tirpassign.edit', compact('tps','busInfo', 'routeInfo'));
    }

    public function update(Request $request, TripAssign $tripAssign)
    {
        $id = decryptIt($request->id);
        $ta = $tripAssign->find($id);
        // $ta->bus_reg_id = decryptIt($request->bus_reg_id);
        $ta->trip_route_id = decryptIt($request->trip_route_id);
        $ta->startPoint = $request->startPoint;
        $ta->endPoint = $request->endPoint;
        $ta->startDate = $request->startDate;
        $ta->endDate = $request->endDate;
        $ta->status = $request->status;
        $result =  $ta->save();
        if ($result) {
            return redirect()->route('trip-assign.index')->with('success', 'Trip Assign Added Successfully!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    }

    public function destroy(TripAssign $tripAssign)
    {
        //
    }

}
