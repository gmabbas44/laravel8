<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Coachfacility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Company;
use Database\Seeders\CoachFacilitySeeder;

class CoachfacilityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index( Coachfacility $coachfacility )
    {
        if( is_super_admin() ) {
            $coachfacilties = $coachfacility->getAll();
        } else {
            $coachfacilties = $coachfacility->thisCoachFacelity( Auth::user()->company_id );
        }
        return view('admin.coach-facility-list', compact('coachfacilties'));
    }

    public function create()
    {
        $companies = Company::select('id','companyName')->get();
        return view('admin.add-coach-facility', compact('companies'));
    }
    
    public function save(Request $request, Coachfacility $coachfacility )
    {
        if( is_super_admin() ){
            $request->validate([
                'title' => 'required',
                'details' => 'required',
                'status' => 'required',
                'company' => 'required'
            ]);
        } else {
            $request->validate([
                'title' => 'required',
                'details' => 'required',
                'status' => 'required'
            ]);
        }
        $coachfacility->title = $request->input('title');
        $coachfacility->details = $request->input('details');
        $coachfacility->status = $request->input('status');
        $coachfacility->company_id = is_super_admin() ? $request->input('company') : Auth::user()->company_id ;
        $res = $coachfacility->save();
        if ($res) {
            return redirect()->route('admin.coach.facelity.list')->with('success', 'Coach Facelity Successfully Added!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    }
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coachfacility  $coachfacility
     * @return \Illuminate\Http\Response
     */
    public function show(Coachfacility $coachfacility)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Coachfacility  $coachfacility
     * @return \Illuminate\Http\Response
     */
    public function edit(Coachfacility $coachfacility)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Coachfacility  $coachfacility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coachfacility $coachfacility)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coachfacility  $coachfacility
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coachfacility $coachfacility)
    {
        //
    }
}
