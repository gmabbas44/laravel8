<?php

namespace App\Http\Controllers\Admin;

use App\Models\TripRoute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Counter;
use App\Models\Location;
use Illuminate\Support\Facades\Auth;

class TripRouteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        if (is_super_admin()) {
            $routes = TripRoute::getAll();
        } else {
            $routes = TripRoute::all()->where('company_id', '=', Auth::user()->company_id);
        }
        return view('admin.route.list', compact('routes'));
    }

    public function create()
    {
        $company_id = Auth::user()->company_id;
        if (is_super_admin()) {
            $locations = [];
            $counters = [];
        } else {
            $locations = Location::select('id', 'locationName')->where([['soft_delete', '=', 0], ['company_id', '=', $company_id]])->get();
            $counters = Counter::select('id', 'counterName')->where([['soft_delete', '=', 0], ['status', '=', 1], ['company_id', '=', $company_id]])->get();
        }
        $companies = Company::select('id', 'companyName')->get();

        // return Counter::whereIn('id', [1, 5, 6])->get();

        return view('admin.route.create', compact('locations', 'companies', 'counters'));
    }

    public function getCompanyRouteInfo(Request $request)
    {
        $company_id = $request->input('company_id');

        $data['routes'] = Location::select('id', 'locationName')->where([['company_id', '=', $company_id], ['soft_delete', '=', 0]])->get();
        $data['counter'] = Counter::select('id', 'counterName')->where([['company_id', '=', $company_id], ['soft_delete', '=', 0], ['status', '=', 1]])->get();
        return response()->json($data);
    }

    public function save(Request $request, TripRoute $tripRoute)
    {
        $request->validate([
            'routeName' => 'required',
            'startLocation' => 'required',
            'endLocation' => 'required',
            'endPointOf' => 'required',
            'stoppage' => 'required',
            'distance' => 'required',
            'approximateTime' => 'required',
            'status' => 'required',
        ]);

        // return $request->input();

        $routename = explode('-', $request->input('routeName'));
        $endPoints = $request->input('endPointOf');
        $endPointOf = "";

        foreach ($endPoints as $ep) {
            $endPointOf .= decryptIt($ep) . ',';
        }
        $stoppage = implode(', ', $request->input('stoppage'));

        $tripRoute->company_id = is_super_admin() ?  $request->input('company') : Auth::user()->company_id;
        $tripRoute->routeName = $request->input('routeName');
        $tripRoute->startLocationId = decryptIt($request->input('startLocation'));
        $tripRoute->startLocationName = $routename[0];
        $tripRoute->endLocationId = decryptIt($request->input('endLocation'));
        $tripRoute->endLocationName = $routename[1];
        $tripRoute->endPointOf = $endPointOf;
        $tripRoute->stoppage = $stoppage;
        $tripRoute->distance = $request->input('distance');
        $tripRoute->approximateTime = $request->input('approximateTime');
        $tripRoute->status = $request->input('status');
        $result = $tripRoute->save();

        if ($result) {
            return redirect()->route('admin.route.list')->with('success', 'Route Successfully Added!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TripRoute  $tripRoute
     * @return \Illuminate\Http\Response
     */
    public function show(TripRoute $tripRoute)
    {
        //
    }


    public function edit(TripRoute $tripRoute, $id)
    {
        $route_id = decryptIt($id);
        $tr = $tripRoute->find($route_id);
        $locations = Location::select('id', 'locationName')
            ->where('soft_delete', '=', 0)
            ->where('company_id', '=', $tr->company_id)
            ->get();
        return view('admin.route.edit', compact('locations', 'tr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TripRoute  $tripRoute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TripRoute $tripRoute)
    {
        //
    }

    public function soft_delete($id)
    {
        $route_id = decryptIt($id);
        $r_info = TripRoute::find($route_id);
        $r_info->soft_delete = 1;
        $result = $r_info->save();
        if ($result) {
            return redirect()->route('admin.route.list')->with('success', 'Route Soft delete Successfully!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TripRoute  $tripRoute
     * @return \Illuminate\Http\Response
     */
    public function destroy(TripRoute $tripRoute)
    {
    }

    public function getCompanyRoute(Request $request)
    {
        $tirpRouteId = decryptIt($request->input('tirpRouteId'));
        $routeInfo = TripRoute::find($tirpRouteId)->first();


        $data['startPoint'] = Counter::select('id', 'counterName')
            ->where('company_id', '=', $routeInfo->company_id)
            ->where('location_id', '=', $routeInfo->startLocationId)
            ->get();
        $data['endPoint'] = Counter::select('id', 'counterName')
            ->where('company_id', '=', $routeInfo->company_id)
            ->where('location_id', '=', $routeInfo->endLocationId)
            ->get();
        return response()->json($data);
        // return response()->json($routeInfo);
    }

    public function getStoppage(Request $request)
    {
        $tirpRouteId = decryptIt($request->input('route_id'));
        $stoppage= TripRoute::select('startLocationName', 'endPointOf')->where('id', '=', $tirpRouteId)->first();
        $location = explode(',', $stoppage->endPointOf);

        $data['endPointOf'] = Location::select('locationName')->whereIn('id', $location)->get();
        return response()->json($data);
    }
}
