<?php

namespace App\Http\Controllers\Admin;

use App\Models\TicketPrice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CoachType;
use App\Models\Company;
use App\Models\TripRoute;
use Illuminate\Support\Facades\Auth;

class TicketPriceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $ticketPrices = TicketPrice::getAll();
        return view('admin.ticketprice.list', compact('ticketPrices'));
    }

    public function getCompanyRouteCoach(Request $request)
    {
        $company_id = $request->input('company_id');
        $data['routeName'] = TripRoute::select('id', 'routeName')->where([['company_id', '=', $company_id],['status', '=', 1], ['soft_delete', '=', 0]])->get();
        $data['coachTypes'] = CoachType::select('id', 'title')->where('company_id', '=', $company_id)->where('status', '=', 1)->get();
        return response()->json($data);
    }

    public function create()
    {
        $id = Auth::user()->company_id;
        $companies = Company::select('id', 'companyName')->get();

        if (is_super_admin()) {
            $companies = Company::select('id', 'companyName')->get();
            $routeName = [];
            $coachTypes = [];
        } else {
            $company_id = [];
            $routeName = TripRoute::select('id', 'routeName', 'stoppage')->where('company_id', '=', $id)->where('status', '=', 1)->where('soft_delete', '=', 0)->get();
            
            $coachTypes = CoachType::select('id', 'title')->where('company_id', '=', $id)->where('status', '=', 1)->get();
        }
        
        return view('admin.ticketprice.create', compact('companies', 'routeName', 'coachTypes'));
    }

    public function store(Request $request, TicketPrice $ticketPrice)
    {
        $request->validate([
            'trip_route'    => 'required',
            'coach_type'    => 'required',
            'price'         => 'required'
        ]);

        $ticketPrice->company_id    = is_super_admin() ? $request->input('company') : Auth::user()->company_id;
        $ticketPrice->trip_route_id = decryptIt($request->input('trip_route'));
        $ticketPrice->coach_type_id = $request->input('coach_type');
        $ticketPrice->endPointOf      = $request->input('endPointOf');
        $ticketPrice->price         = $request->input('price');
        $result = $ticketPrice->save();

        if ($result) {
            return redirect()->route('ticket-price.index')->with('success', 'Ticket Price Added Successfully!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    }


    public function show(TicketPrice $ticketPrice)
    {
        //
    }

    public function edit($id, TicketPrice $ticketPrice)
    {
        $tp_Id = decryptIt($id);
        
        $ticketPriceId = $ticketPrice->find($tp_Id)->first();
        $id =  $ticketPriceId->company_id;
        
        $routeName = TripRoute::select('id', 'routeName')->where('company_id', '=', $id)->where('status', '=', 1)->where('soft_delete', '=', 0)->get();
        $coachTypes = CoachType::select('id', 'title')->where('company_id', '=', $id)->where('status', '=', 1)->get();
        
        return view('admin.ticketprice.edit', compact('routeName', 'coachTypes', 'ticketPriceId' ));
    }

    public function update(Request $request, TicketPrice $ticketPrice)
    {
        $request->validate([
            'trip_route'    => 'required',
            'coach_type'    => 'required',
            'stoppage'      => 'required',
            'price'         => 'required'
        ]);
        $id = decryptIt($request->id);
        $getResult = $ticketPrice->find($id);

        $getResult->trip_route_id = decryptIt($request->trip_route);
        $getResult->coach_type_id = $request->coach_type;
        $getResult->stoppage      = $request->stoppage;
        $getResult->price         = $request->price;
        $result = $getResult->save();

        if ($result) {
            return redirect()->route('ticket-price.index')->with('success', 'Ticket Price Update Successfully!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TicketPrice  $ticketPrice
     * @return \Illuminate\Http\Response
     */
    public function destroy(TicketPrice $ticketPrice)
    {
        //
    }

    public function getticketprice(Request $request, TicketPrice $ticketPrice)
    {
        $dropingPoint =  $request->input('dropingPoint');
        $company_id =  $request->input('company_id');
        $trip_route_id =  $request->input('trip_route_id');

        $getprice = TicketPrice::select('price')
                        ->where([
                            ['company_id', '=', $company_id],
                            ['trip_route_id', '=', $trip_route_id],
                            ['endPointOf', '=', $dropingPoint]
                        ])->first();
        return response()->json($getprice);
    }
}
