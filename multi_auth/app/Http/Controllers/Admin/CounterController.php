<?php

namespace App\Http\Controllers\Admin;

use App\Models\Counter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Location;
use Illuminate\Support\Facades\Auth;


class CounterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        if (is_super_admin()) {
            $counters = Counter::getAll();
        } else {
            $counters = Counter::where('company_id', '=', Auth::user()->company_id)->get();
        }
        return view('admin.counter.list', compact('counters'));
    }

    public function create()
    {
        $companies = Company::select('id', 'companyName')->get();
        if (is_super_admin()) {
            $locations = [];
        } else {
            $locations = Location::select('id', 'locationName')->where('company_id', Auth::user()->company_id)->get();
        }
        return view('admin.counter.create', compact('companies', 'locations'));
    }

    public function store(Request $request, Counter $counter)
    {
        $request->validate([
            'location' => 'required',
            'counterName' => 'required',
            'status' => 'required',
            'address' => 'required',
        ]);

        // return $request->input();
        // exit;
        $counter->company_id = is_super_admin() ? $request->company : Auth::user()->company_id;
        $counter->location_id = decryptIt($request->location);
        $counter->counterName = $request->counterName;
        $counter->address = $request->address;
        $counter->status = $request->status;
        $result = $counter->save();
        if ($result) {
            return redirect()->route('counter.index')->with('success', 'Counter Successfully Added!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    }

    public function show(Counter $counter)
    {
        //
    }


    public function edit($id)
    {
        $counter_id = decryptIt($id);

        $counter = Counter::find($counter_id);

        $company_locations = Location::select('id', 'locationName')
            ->where('company_id', '=', $counter->company_id)
            ->get();
        if (is_super_admin()) {
            $locations = [];
        } else {
            $locations = Location::select('id', 'locationName')->where('company_id', Auth::user()->company_id)->get();
        }
        return view('admin.counter.edit', compact('company_locations', 'counter'));
    }

    public function update($id, Request $request, Counter $c )
    {
        $request->validate([
            'location_id' => 'required',
            'counterName' => 'required',
            'status' => 'required',
            'address' => 'required'
        ]);
        $counter_id = decryptIt($id);
        $counter = $c->findOrFail($counter_id);
        return $c;
        // $counter->location_id = decryptIt($request->location);
        // $counter->counterName = $request->counterName;
        // $counter->address = $request->address;
        // $counter->status = $request->status;
        $input = $request->all();
        return $input;

        $result = $counter->save();
        // dd($result);
        if ($result) {
            return redirect()->route('counter.index')->with('success', 'Counter Successfully Updated!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Counter  $counter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Counter $counter)
    {
        //
    }
}
