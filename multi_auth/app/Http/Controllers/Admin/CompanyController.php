<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use Illuminate\Support\Facades\Auth;


class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->userAccess();
    }

    private function userAccess()
    {
        
        // if( Auth::user()->user_type_id !== 1 || Auth::user()->user_type_id !== 2) {
        //     return redirect()->route('admin.route');
        // }
        
    }
    public function index()
    {
        $companies = Company::all();
        return view('admin.company-list', ['companies' => $companies]);
    }

    public function create()
    {
        return view('admin.add-company');
    }

    public function save(Request $request)
    {
        $request->validate([
            'companyName' => 'required',
            'companyEmail' => 'required',
            'mobile' => 'required',
            'status' => 'required',
        ]);

        $com = new Company();
        $com->companyName = $request->input('companyName');
        $com->companyEmail = $request->input('companyEmail');
        $com->mobile = $request->input('mobile');
        $com->status = $request->input('status');
        $res = $com->save();

        if ($res) {
            return redirect()->route('admin.company.list')->with('success', 'Company Added Successfully!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    }

    public function delete( $id )
    {
        echo $id;
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $company = Company::find($id);

        return view('admin.edit-company', compact('company'));

    }

    public function update(Request $request, Company $company)
    {
        $request->validate([
            'companyName' => 'required',
            'companyEmail' => 'required',
            'mobile' => 'required',
            'status' => 'required',
        ]);

        // $company-name

        return $request;
    }

    public function destroy($id)
    {
        //
    }
}
