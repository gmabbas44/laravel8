<?php

namespace App\Http\Controllers\Admin;

use App\Models\Driver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Support\Facades\Auth;

class DriverController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (is_super_admin()) {
            $drivers = Driver::getAll();
        } else {
            $drivers = Driver::where('company_id', '=', Auth::user()->company_id)->where('soft_delete', '=', 0)->get();
        }
        return view('admin.driver.list', compact('drivers'));
    }

    public function create()
    {
        $companies = Company::select('id', 'companyName')->get();
        return view('admin.driver.create', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    public function save(Request $request, Driver $driver)
    {
        if (is_super_admin()) {
            $request->validate([
                'driverName' => 'required',
                'driverPhone' => 'required',
                'nidNo' => 'required',
                'company' => 'required',
                'status' => 'required',
            ]);
        } else {
            $request->validate([
                'driverName' => 'required',
                'driverPhone' => 'required',
                'nidNo' => 'required',
                'status' => 'required',
            ]);
        }
        $driver->driverName = $request->input('driverName');
        $driver->driverPhone = $request->input('driverPhone');
        $driver->nidNo = $request->input('nidNo');
        $driver->joinDate = now();
        $driver->company_id = is_super_admin() ? $request->input('company') : Auth::user()->company_id;
        $driver->status = $request->input('status');
        $result = $driver->save() ;
        if ( $result ) {
            return redirect()->route('admin.driver.list')->with('success', 'Driver Added Successfully!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function show(Driver $driver)
    {
        //
    }

    public function edit(Driver $driver, $id)
    {
        $id = decryptIt( $id );
        $driverInfo = $driver->find($id);
        $companies = Company::select('id', 'companyName')->get();
        return view('admin.driver.edit', compact('driverInfo', 'companies'));
    }

    public function update(Request $request, Driver $driver)
    {
        $id = decryptIt( $request->id );
        $update = $driver->find( $id );
        $update->driverName = $request->driverName;
        $update->driverPhone = $request->driverPhone;
        $update->nidNo = $request->nidNo;
        $update->drivingLicenseNo = $request->drivingLicenseNo;
        $update->company_id = is_super_admin() ?  $request->company : Auth::user()->company_id;
        $update->status = $request->status;
        if ( is_super_admin() ) {
            $update->soft_delete =  $request->is_delete ;
        }
        $result =  $update->save();

        if ($result) {
            return redirect()->route('admin.driver.list')->with('success', 'Bus Information Update Successfully!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    }
    public function delete($id)
    {
        $id = decryptIt( $id );
        $softDelete = Driver::find( $id );
        $softDelete->soft_delete = 1;
        $result = $softDelete->save();
        if ($result) {
            return redirect()->route('admin.driver.list')->with('success', 'Bus Information Delete Successfully!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy(Driver $driver)
    {
        //
    }
}
