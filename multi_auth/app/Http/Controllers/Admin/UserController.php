<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\User;
use App\Models\UserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $user = new User();
        if ( is_super_admin() ) {
            $all =  $user->allUsers();
        } else {
            $all = $user->companyAdmin(Auth::user()->company_id);
        }

        return view('admin.admin-list', ['all' => $all]);
    }

    public function create()
    {
        if ( is_super_admin() ) {
            $usertypes = UserType::select('id', 'title')
                ->where('status', '=', '1')
                ->get();
        } else {
            $usertypes = UserType::select('id', 'title')
                ->where('status', '=', '1')
                ->where('id', '>', 2)
                ->get();
        }
        $companies = Company::select('id', 'companyName')->where('status', '=', '1')->get();

        return view('admin.create-admin', ['companies' => $companies, 'usertypes' => $usertypes]);
    }

    public function store(Request $request)
    {
        //
    }
    public function save(Request $request)
    {
        if ( is_super_admin() ) {
            $request->validate([
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'mobile' => 'required|min:11',
                'user_type_id' => 'required',
                'company_id' => 'required',
                'password' => 'required|min:8'
            ]);
        }else {
            $request->validate([
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'mobile' => 'required|min:11',
                'user_type_id' => 'required',
                'password' => 'required|min:8'
            ]);
        }

        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->mobile = $request->input('mobile');
        $user->user_type_id = $request->input('user_type_id');
        $user->company_id = is_super_admin() ? $request->input('company_id') : Auth::user()->company_id;
        $user->password = Hash::make($request->input('password'));
        $res = $user->save();
        if ($res) {
            return redirect()->route('admin.list')->with('success', 'Admin Added Successfully!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }

        return $request->input();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
