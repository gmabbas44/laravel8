<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Location;
use App\Models\TripAssign;
use Illuminate\Http\Request;

class TripManageContoller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.tripmanage.list');
    }
    public function gettriplist(Request $request)
    {
        $startLocation   = $request->input('startLocation');
        $endLocation     = $request->input('endLocation');
        $bookingDate     = $request->input('bookingDate');

        //    $data = [$startLocation, $endLocation, $bookingDate];

        if ($startLocation !== null && $endLocation !== null && $bookingDate !== null) {
            
        } else {
            $data = TripAssign::getAllAssign();
        }

        //    $data = TripAssign::all();
        return response()->json($data);
    }

    public function seat_view($id)
    {
        $assing_id =  decryptIt($id);

        $getData = TripAssign::select('companies.companyName', 'trip_assigns.company_id', 'trip_assigns.trip_route_id', 'trip_routes.routeName', 'bus_regs.totalSeat', 'bus_regs.layout', 'trip_routes.endPointOf')
        ->join('companies', 'companies.id', '=', 'trip_assigns.company_id')
        ->join('trip_routes', 'trip_routes.id', '=', 'trip_assigns.trip_route_id')
        ->join('bus_regs', 'bus_regs.id', '=', 'trip_assigns.bus_reg_id')
        ->where('trip_assigns.id', '=', $assing_id )->first();

        $location = explode(',', $getData->endPointOf);

        $endPointOf = Location::select('id', 'locationName')->whereIn('id', $location)->get();

        $seatStatus = [
            'seat_empty_color'      => 'Empty Seat', 
            'seat_selected_color'   => 'Selected Seat',
            'seat_booked_color'     => 'Seat Booked',
            'seat_confirm_color'    => 'Seat Confirm',
            'seat_female_color'     => 'Female Confirm'
        ];
        
        return view('admin.seatview.view', compact('getData', 'endPointOf', 'seatStatus'));
    }
}
