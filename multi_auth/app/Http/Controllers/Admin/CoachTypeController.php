<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CoachType;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CoachTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(CoachType $coachType)
    {
        if( is_super_admin() ) {
            $coachTypes =  $coachType->getAll();
        }else {
            $coachTypes =  $coachType->thisCoachType( Auth::user()->company_id);
        }
        return view('admin.coach-type-list', compact('coachTypes'));
    }

    public function create()
    {
        $companies = Company::select('id','companyName')->get();
        return view('admin.add-coach-type', compact('companies'));
    }

    public function save( Request $request, CoachType $coachType)
    {
        if( is_super_admin() ){
            $request->validate([
                'title' => 'required',
                'details' => 'required',
                'status' => 'required',
                'company' => 'required'
            ]);
        } else {
            $request->validate([
                'title' => 'required',
                'details' => 'required',
                'status' => 'required'
            ]);
        }
        $coachType->title = $request->input('title');
        $coachType->details = $request->input('details');
        $coachType->status = $request->input('status');
        $coachType->company_id = is_super_admin() ? $request->input('company') : Auth::user()->company_id;
        $res = $coachType->save();
        if ($res) {
            return redirect()->route('admin.coach.type.list')->with('success', 'Coach Type Successfully Added!');
        } else {
            return back()->with('warning', 'Something Wrong, Please Try again');
        }
        
    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
