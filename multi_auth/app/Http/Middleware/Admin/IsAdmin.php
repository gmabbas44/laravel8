<?php

namespace App\Http\Middleware\Admin;

use Closure;
use Illuminate\Http\Request;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ( auth()->user()->user_type_id <= 4) {
            return $next($request);
        }
        return redirect('login')->with('error',"You don't have admin access."); 
    }
}
