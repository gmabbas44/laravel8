<?php 

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
function is_super_admin()
{
    if (Auth::user()->user_type_id <= 2) {
        $result = true;
    } else {
       $result = false;
    }
    return $result;
}
function companyinfo($company_id) {
    $company =  DB::table('companies')->where('id', '=', $company_id)->first();
    // return  $company->companyName; 
    return $company;
    
}