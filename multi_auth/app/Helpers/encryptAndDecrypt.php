<?php

function decryptIt($string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
    $secret_iv = 'This is my secret iv';
    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    return $output;
}

function encryptIt($string)
{
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
    $secret_iv = 'This is my secret iv';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    
    $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
    $output = base64_encode($output);
    $output=str_replace("=", "", $output);
    
    return $output;
}