<?php


function status($status)
{
    if ( $status == 0 ) {
        $result = '<span class="badge badge-pill badge-danger">Deactive</span>';
    } else {
        $result = '<span class="badge badge-pill badge-primary">Active</span>';
    }
    echo $result;
    
}

function is_deleted( $value )
{
    if ( $value == 1 ) {
        $result = '<span class="badge badge-pill badge-primary">Yes</span>';
    } else {
        $result = '<span class="badge badge-pill badge-danger">NO</span>';
    }
    echo $result;
}