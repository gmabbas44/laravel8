@extends('superadmin/admin-layout')

@section('main_content')
<h3>Trip Type list</h3>

<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <th>Sl</th>
            <th>Type name</th>
            <th>Company</th>
            <th>Status</th>
            <th>Action</th>
        </thead>    
        <tbody>
        @foreach( $trip_types as $tp)
            <tr>
                <td>{{ $tp->id }}</td>
                <td>{{ $tp->trip_name }}</td>
                <td>{{ $tp->company_id }}</td>
                <td class="text-center">
                    @if( $tp->status == 1)
                    <span class="badge badge-success">Active</span>
                    @else
                    <span class="badge badge-danger">Deactive</span>
                    @endif
                </td>
                <td>
                    <a href="edit-trip-type/{{ $tp->id }}"><i class="fa fa-edit text-primary"></i></a> ||
                    <a onclick="return confirm('Are you sure?')" href="#"><i class="fa fa-trash text-danger"></i></a> 
                </td>
            </tr>
        @endforeach
        </tbody>
        
    </table>
</div>

@endsection


@section('custom_js')

<!-- seript here -->

@endsection