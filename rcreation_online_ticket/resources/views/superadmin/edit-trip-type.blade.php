@extends('superadmin/admin-layout')

@section('main_content')
<h3>Edit Trip New Type</h3>

<div class="card card-body">

    <form action="/update-trip-type" method="POST">
        @csrf
        <input type="hidden" name="id" value="{{$trip_data->id}}">
        <div class="row mb-3">
            <div class="col">
                <label for="name">Tirp type name</label>
                <input type="text" name="trip_name" class="form-control" placeholder="First name" value="{{$trip_data->trip_name}}" id="name">
            </div>
            <div class="col">
                <label for="company">Company</label>
                <select class=form-control name="company_id" value="" id="company">
                   @foreach( $companies as $company)
                    <?php 
                        
                    if( $company->id == $trip_data->company_id):
                        $select = 'selected';
                    else :
                        $select = '';
                    endif;

                    ?>
                    <option {{$select}} value="{{$company->id}}">{{ $company->company_name }}</option>
                   @endforeach
                </select>
            </div>
            <div class="col">
                <label for="status">Status</label>
                <select class=form-control name="status" id="status">
                    <?php 
                        
                        if( $trip_data->status == 1):
                            $selected = 'selected';
                        else :
                            $selected = '';
                        endif;

                    ?>
                    <option {{ $selected }} value="1">Active</option>
                    <option {{$selected}} value="0">Deactive</option>
                    
                </select>
            </div>
        </div>
        <button class="btn btn-primary btn-sm">Save</button>
    </form>

</div>

@endsection


@section('custom_js')

<!-- seript here -->

@endsection