@extends('superadmin/admin-layout')

@section('main_content')
<h2>Add Super Admin</h2>

<div class="card">
    <div class="card-body">
        <form action="/add-new-supper-admin" method="POST" enctype="multipart/form-data">
        @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Full name">
                </div>
                <div class="form-group col-md-6">
                    <label for="username">Username</label>
                    <input type="text" name="username" class="form-control" id="username" placeholder="User name">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Email">
                </div>
                <div class="form-group col-md-6">
                    <label for="mobile">Mobile</label>
                    <input type="number" name="mobile" class="form-control" id="mobile" placeholder="Nobile no">
                </div>
            </div>
            
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="image">Admin Photo</label>
                    <input type="file" name="image" class="form-control" id="image">
                </div>
                <div class="form-group col-md-4">
                    <label for="status">Status</label>
                    <select id="status" name="status" class="form-control">
                        <option selected>Choose...</option>
                        <option value='1'>Active</option>
                        <option value='0'>Deactive</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                </div>
            </div>
            
            <button type="submit" class="btn btn-primary">Add</button>
        </form>
    </div>
</div>

@endsection


@section('custom_js')

<!-- seript here -->

@endsection