@extends('superadmin/admin-layout')

@section('main_content')
<h3>Add Trip New Type</h3>

<div class="card card-body">

    <form action="save-trip-type" method="POST">
        @csrf
        <div class="row mb-3">
            <div class="col">
                <label for="name">Tirp type name</label>
                <input type="text" name="trip_name" class="form-control" placeholder="First name" id="name">
            </div>
            <div class="col">
                <label for="company">Company</label>
                <select class=form-control name="company_id" id="company">
                    <option value="#">Select one</option>
                    @foreach( $companies as $company)
                    <option value="{{ $company->id }}">{{ $company->company_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col">
                <label for="status">Status</label>
                <select class=form-control name="status" id="status">
                    <option value="1">Active</option>
                    <option value="0">Deactive</option>
                </select>
            </div>
        </div>
        <button class="btn btn-primary btn-sm">Save</button>
    </form>

</div>

@endsection


@section('custom_js')

<!-- seript here -->

@endsection