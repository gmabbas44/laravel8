@extends('superadmin/admin-layout')

@section('main_content')
<h3>Super Admin</h3>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <th>Sl</th>
            <th>User name</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Photo</th>
            <th>Status</th>
            <th>Action</th>
        </thead>
        <tbody>
        @foreach( $superAdmins as $sa)
            <tr>
                <td>{{$sa->id}}</td>
                <td>{{$sa->username}}</td>
                <td>{{$sa->email}}</td>
                <td>{{$sa->mobile}}</td>
                <td>{{$sa->image}}</td>
                <td>{{$sa->status}}</td>
                <td><a href="super-admin-update/{{$sa->id}}"><i class="fa fa-edit"></i></a>|| <a href="super-admin-delete/{{$sa->id}}"><i class="fa fa-trash text-danger"></i></a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection


@section('custom_js')

<!-- seript here -->

@endsection