<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;


class Triptype extends Model
{
    use HasFactory;
    protected $table = 'trip_types';

    public static function find_data($id)
    {
        return DB::table('trip_types')
                    ->join('companies', 'company_id', '=', 'companies.id')
                    ->select('trip_types.id', 'trip_name', 'companies.company_name', 'trip_types.company_id', 'trip_types.status')
                    ->where('trip_types.id', '=', $id)
                    ->first();
    }
}
