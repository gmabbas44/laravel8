<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Superadmin;
use Illuminate\Support\Facades\Hash;

class SuperadminController extends Controller
{
    public function admin_login( Request $req)
    {
        $req->input();
        $superAdmin = Superadmin::where(['mobile'=>$req->mobile])->first();
        if ( $superAdmin && Hash::check($req->password, $superAdmin->password)) {
            $req->session()->put('supper_admin', $superAdmin);
            return \redirect('super-admin-deshboard');
        }else {
            return \redirect('admin');
        }
       // return $supperAdmin;
    }
    
    public function deshboard( Request $req)
    {
        $super_admin = $req->session()->get('supper_admin');
        return \view('superadmin/deshboard', ['super_admin'=> $super_admin]); 
    }

    public function superAdminList()
    {
        $data = Superadmin::all();
        return view('superadmin/super-admin-list',['superAdmins' => $data]);
    }
    
    public function addSuperAdmin()
    {
        return view('superadmin/add-super-admin');
    }

    public function addNewSuperAdmin( Request $req)
    {
        $sa = new Superadmin();

        $sa->name = $req->input('name');
        $sa->username = $req->input('username');
        $sa->email = $req->input('email');
        $sa->mobile = $req->input('mobile');
        $sa->image = $req->file('image')->store('img/super_admin');
        $sa->status = $req->input('status');
        $sa->password = Hash::make($req->input('password'));
        $result = $sa->save();
        if ( $result ) {
            return \redirect('super-admin-list');
        }
    }

    public function logout()
    {
        # code...
    }

}
