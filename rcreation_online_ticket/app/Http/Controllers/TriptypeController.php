<?php

namespace App\Http\Controllers;

use App\Models\Triptype;
use App\Models\Company;
use Illuminate\Http\Request;


class TriptypeController extends Controller
{
    public function index()
    {
        
    }

    
    public function create( Request $request)
    {
        $triptype = new Triptype;
        $triptype->trip_name = $request->trip_name;
        $triptype->company_id = $request->company_id;
        $triptype->status = $request->status;
        $results = $triptype->save();
        if( $results) {
            return redirect('view-trip-type');
        }
    }

   
    public function store(Request $request)
    {
        //
    }

    
    public function show(Triptype $triptype)
    {
        $trip_types = Triptype::all();
        return view('superadmin/trip-type-list', ['trip_types' => $trip_types]);
    }

    public function add()
    {
        $companies = Company::all();
        return view('superadmin/add-trip-type', ['companies'=> $companies]);
    }

    public function edit($id)
    {
        $data['companies'] = Company::all();
        $data['trip_data'] = Triptype::find_data($id);
        return view('superadmin/edit-trip-type', $data);
    }

    public function update(Request $request, Triptype $triptype)
    {
        $tp = Triptype::find($request->id) ;
        $tp->trip_name = $request->trip_name;
        $tp->status = $request->status;
        $tp->company_id = $request->company_id;
        $result = $tp->save();
        if( $result ) {
            return \redirect('view-trip-type');
        }
        
    }

    public function destroy(Triptype $triptype)
    {
        //
    }
}
