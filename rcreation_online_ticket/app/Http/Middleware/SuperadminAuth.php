<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class SuperadminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if( $request->path() == 'super-admin' && $request->session()->has('supper_admin')) {
            return \redirect('super-admin-deshboard');
        }
        return $next($request);
    }
}
