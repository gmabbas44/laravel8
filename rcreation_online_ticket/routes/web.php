<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\SuperadminController;
use \App\Http\Controllers\CompanyController;
use \App\Http\Controllers\AdminController;
use \App\Http\Controllers\TriptypeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// for super Admin
Route::get('super-admin', function() {
    return view('admin_login');
});
Route::post('/super-admin-login',[SuperadminController::Class, 'admin_login']);
Route::get('super-admin-deshboard',[SuperadminController::Class, 'deshboard']);
Route::get('super-admin-list',[SuperadminController::class, 'superAdminList']);
Route::get('add-super-admin',[SuperadminController::class, 'addSuperAdmin']);
Route::post('add-new-supper-admin',[SuperadminController::class, 'addNewSuperAdmin']);

// for super admin trip type controll
Route::get('view-trip-type', [TriptypeController::class, 'show']);
Route::get('add-trip-type', [TriptypeController::class, 'add']);
Route::post('save-trip-type', [TriptypeController::class, 'create']);
Route::get('edit-trip-type/{id}', [TriptypeController::class, 'edit']);
Route::post('update-trip-type', [TriptypeController::class, 'update']);

// for company
Route::get('company', function (){
    return view('company/company-login');
});
Route::post('company_login', [CompanyController::class, 'login']);
Route::get('company-deshboard', [CompanyController::class, 'index']);

// for admin
Route::get('admin', function (){
    return view('admin/admin-login');
});
Route::post('admin_login', []);