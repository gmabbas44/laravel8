<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class Admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'username' => 'Admin',
            'email' => 'Admin@gmail.com',
            'mobile' => '01845109044',
            'status' => 1,
            'password' => Hash::make(1234),
            'access_rule_id' => 2,
            'company_id' => 1
        ]);
    }
}
