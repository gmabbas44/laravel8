<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AccessRules extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('access_rules')->insert([
            'rule_name' => 'Super Admin',
            'description' => 'Control Full System',
            'status' => 1
        ]);
        
    }
}
