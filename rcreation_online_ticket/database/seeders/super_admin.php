<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class super_admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('super_admin')->insert([
            'name' => 'Gm Abbas Uddin',
            'username' => 'gmabbas',
            'email' => 'gmabbas44@gmail.com',
            'mobile' => '01845109044',
            'image' => uniqid().'.jpg',
            'status' => '1',
            'password' => Hash::make(1234),
            'remember_token' => uniqid()
        ]);
    }
}
