<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class Company extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->delete();
        DB::table('companies')->insert([
            [
                'company_name' => 'MD. Abbas Uddin',
                'username' => 'abbasuddin',
                'email' => 'abbasuddin@gmil.com',
                'mobile' => '01981781661',
                'status' => '1',
                'password' => Hash::make(1234),
                'add_by' => 'super admin',
                'super_admin_id' => 1
            ],
            [
                'company_name' => 'Company',
                'username' => 'Company',
                'email' => 'company@email.com',
                'mobile' => '01845109044',
                'status' => '1',
                'password' => Hash::make(1234),
                'add_by' => 'super admin',
                'super_admin_id' => 1
            ]]);
    }
}
