<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\users;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ApiUsers;
use App\Http\Controllers\RequestMethod;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\OperationController;
use App\Http\Controllers\Admin\UserApiController;
use App\Models\cource;
use App\Models\Student;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*------------------------------

Route::get('/user/{name}', [users::class, 'show']);

Route::get("/list/{name}", function ($name){
    return view('list',['name' => $name]);
});

Route::get('/userlist/{name}', [users::class, 'userlist']);
Route::get('/com', [users::class, 'component_second']);
Route::get('/view-blade', [users::class, 'view_blade']);
Route::get('/bladetmp', function () {
    $names = ['abbas', 'uddin', 'Khaleda Akter', 'Nodi'];
    return view('bladetemp', ['names' => $names]);
});

// Call view page without controller
Route::view('login','login');
Route::post('userlogin', [users::class, 'userlogin']);

// Group middleware
Route::group(['middleware' => ['protect_page']], function () {
    Route::view('page-1', 'page-1');
    Route::view('page-2', 'page-2');
});

// Route middleware
Route::view('page-3', 'page-3')->middleware('protectedPage');
Route::view('noaccess', 'noaccess');

Route::get('getall', [UserController::class, 'show']);

// Api user list 
Route::get('apiuser', [Apiusers::class, 'index']);

// get data from form data
// Route::view('userlogin', 'userlogin');
Route::post('submitData', [RequestMethod::class, 'getData']);

Route::get('/logout', function() {
    if (session()->has('user')) {
        session()->pull('user', null);
    }
    return redirect('userlogin');
});

Route::get('/userlogin', function() {
    if (session()->has('user')) {
        return redirect('porfile');
        // return view('profile');
    }
    return view('userlogin');
});
Route::get('/profile', function () {
    if ( session()->has('user')) {
        return view('profile');
    }else {
        return redirect('userlogin');
    }
});

// upload image file
Route::view('upload', 'upload');
Route::post('uploadFile', [UploadController::class, 'uploadFile']);

// save data in database
Route::view('add', 'add-form');
Route::post('/save', [UserController::class, 'save']);
Route::get('/delete/{id}', [UserController::class, 'delete']);
Route::get('/edit/{id}', [UserController::class, 'showData']);
Route::post('/update', [UserController::class, 'update']);

// Operation (laravel funciton)
Route::get('/operation', [OperationController::class, 'operation']);


// for one to one relationship
Route::get('student/{id}/cource', function($id) {
    // return Student::all();
    return Student::find( $id )->cource;
});
Route::get('cource/{id}/student', function($id) {
    // return Student::all();
    return cource::find( $id )->student;
});

// for one to many relationship
Route::get('student/{id}/cources', function ($id) {
    $student = Student::find($id);
    foreach( $student->cources as $cource) {
        echo $cource->cource_name . '<br>';
    }
});

Route::get('subject/insert', function () {
    DB::table('subjects')->insert(
        ['subjectName' => 'ICT']
    );
});



Route::get('key/insert', function () {
    $data = [
        [ 'student_id' => 2, 'subject_id' => 1, 'created_at' => now(), 'updated_at' => now() ],
        [ 'student_id' => 2, 'subject_id' => 2, 'created_at' => now(), 'updated_at' => now() ],
        [ 'student_id' => 2, 'subject_id' => 3, 'created_at' => now(), 'updated_at' => now() ],
        [ 'student_id' => 2, 'subject_id' => 4, 'created_at' => now(), 'updated_at' => now() ],
        [ 'student_id' => 4, 'subject_id' => 1, 'created_at' => now(), 'updated_at' => now() ],
        [ 'student_id' => 4, 'subject_id' => 2, 'created_at' => now(), 'updated_at' => now() ],
        [ 'student_id' => 4, 'subject_id' => 3, 'created_at' => now(), 'updated_at' => now() ],
        [ 'student_id' => 4, 'subject_id' => 4, 'created_at' => now(), 'updated_at' => now() ],
        [ 'student_id' => 8, 'subject_id' => 1, 'created_at' => now(), 'updated_at' => now() ],
        [ 'student_id' => 8, 'subject_id' => 1, 'created_at' => now(), 'updated_at' => now() ],
        [ 'student_id' => 8, 'subject_id' => 2, 'created_at' => now(), 'updated_at' => now() ],
        [ 'student_id' => 8, 'subject_id' => 3, 'created_at' => now(), 'updated_at' => now() ],
        [ 'student_id' => 9, 'subject_id' => 1, 'created_at' => now(), 'updated_at' => now() ],
        [ 'student_id' => 9, 'subject_id' => 2, 'created_at' => now(), 'updated_at' => now() ],
        [ 'student_id' => 9, 'subject_id' => 3, 'created_at' => now(), 'updated_at' => now() ],
        [ 'student_id' => 9, 'subject_id' => 4, 'created_at' => now(), 'updated_at' => now() ],
    ];
    DB::table('student_subject')->insert( $data );
});

------------------------------*/

Route::get('student/{id}/subjects', function ( $id ) {
    $student = Student::find($id);
    $subs = $student->regs;

    foreach ( $subs as $sub ) {
        echo $sub->subjectName . '<br>';
    }
});



