<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\facades\DB;
use Illuminate\Support\Str;

class empSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('emp')->insert([
            'name' => Str::random(20),
            'email' => Str::random(20).'@gmail.com',
            'password' => md5(Str::random(20)),
        ]);
    }
}
