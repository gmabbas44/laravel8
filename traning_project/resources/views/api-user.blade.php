<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Api user List</h2>
    <table border='2' cellpadding=5>
        <tr>
            <th>Id</th>
            <th>Email</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Image</th>
        </tr>
        @foreach( $apiuserlist as $user )
        <tr>
            <td>{{$user['id']}}</td>
            <td>{{$user['email']}}</td>
            <td>{{$user['first_name']}}</td>
            <td>{{$user['last_name']}}</td>
            <td><img src="{{$user['avatar']}}" alt=""></td>
        </tr>
        @endforeach
    </table>
</body>
</html>