<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<a href="/add">Add new</a>
    <table border="2" cellpadding='5'>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Password</th>
            <th>Action</th>
        </tr>
        @foreach( $users as $u )
        <tr>
            <td>{{$u->id}}</td>
            <td>{{$u->name}}</td>
            <td>{{$u->email}}</td>
            <td>{{$u->password}}</td>
            <td><a href="/edit/{{$u->id}}">Edit</a> || <a href="/delete/{{$u->id}}">Delete</a></td>
        </tr>
        @endforeach
    </table>

    <div class="paginate">
    {{ $users->links() }}
    </div>
</body>
</html>