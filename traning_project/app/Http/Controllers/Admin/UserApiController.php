<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\UserApi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UserApi::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\UserApi  $userApi
     * @return \Illuminate\Http\Response
     */
    public function show(UserApi $userApi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\UserApi  $userApi
     * @return \Illuminate\Http\Response
     */
    public function edit(UserApi $userApi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\UserApi  $userApi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserApi $userApi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\UserApi  $userApi
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserApi $userApi)
    {
        //
    }
}
