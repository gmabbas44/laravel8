<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
use App\Models\User;

class UserController extends Controller
{
    public $id;
    public function show()
    {
        // return DB::select('select * from emp');

        // for all data
        // $data = User::all();

        // for paginate data
        $data = User::paginate(5);

        return view('getall', ['users' => $data ]);
    }
    public function save( Request $req)
    {
        $user = new User;
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = $req->password;
        $results = $user->save();
        if ($results) {
            return \redirect('getall');
        }
        // return $req->input();
    }
    public function delete( $id )
    {
        $result = User::find($id);
        if ( $result ) {
            $result->delete();
            return \redirect('getall');
        } else {
            return \redirect('getall');
        }
    }
    public function showData( $id )
    {
        $result = User::find($id);
        $this->id = $result->id;
        return view('show-data', ['user' => $result]);
    }
    public function update( Request $req )
    {
        $data = User::find( $req->id);
        $data->name = $req->name;
        $data->email = $req->email;
        $data->password = $req->password;
        $r = $data->save();
        if ($r) {
            return \redirect('getall');
        }
        // return $data;
    }

}
