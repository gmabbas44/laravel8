<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class users extends Controller
{
    public function show($name)
    {
        // return view('user');
        return $name;
    }

    public function userlist( $name )
    {
        return view('list',['name' => $name]);
    }
    public function component_second()
    {
        return view('component-2');
    }
    public function view_blade()
    {
        $name = 'Gm Abbas Uddin';
        return view('viewblade',['name'=> $name]);
    }
    public function userlogin(Request $req)
    {
        return $req->input();
    }
}
