<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class OperationController extends Controller
{
    public function operation()
    {
        $user = new User;
        // for get data 
        // return User::all();
        // return DB::table('emp')->get();
        // return $user->all();

        // for count data
        // return DB::table('emp')->count();
        // return $user->count();

        // for find with id, this method will return a arrya
        // return (array)DB::table('emp')->find(5);
        // return $user->find(8);

        // for where condition
        // return DB::table('emp')->where('id', 5)->get();
        // return $user->where('id', 4)->get();

        // for insert data
        // return DB::table('emp')
        //     ->insert([
        //         'name' => 'Gm Abbas Uddin',
        //         'email' => 'gmabbas44@gmail.com',
        //         'password' => md5(12345)
        //     ]);
        // return $user
        //     ->insert([
        //         'name' => 'Khaleda Akter',
        //         'email' => 'khaledaakter382616',
        //         'password' => md5(12345)
        //     ]);
        
        // for update data
        // return DB::table('emp')
        // ->where('id', 5)
        // ->update([
        //     'name' => 'Gm Abbas Uddin',
        //     'email' => 'gmabbas44@gmail.com',
        //     'password' => md5(12345)
        // ]);

        // return $user
        // ->where('id', 11)
        // ->update([
        //     'name' => 'Khaleda Akter',
        //     'email' => 'khaledaakter382616@gmail.com',
        //     'password' => md5(12345)
        // ]);

        // for delete data
        // return DB::table('emp')->where('id', 5)->delete();
        // return $user->where('id', 3)->delete();

        // Aggregate method 
        // return DB::table('emp')->sum('id');
        // return $user->sum('id');

        // return DB::table('emp')->avg('id');
        // return $user->avg('id');

        // return DB::table('emp')->max('id');
        // return $user->max('id');
        
        // return DB::table('emp')->min('id');
        // return $user->min('id');

        // join query
        // return DB::table('cources')
        // ->join('emp', 'cources.emp_id', '=', 'emp.id')
        // ->select('emp.*', 'cources.cource_name')
        // ->get();
        
        // return $user
        // ->join('emp', 'cources.emp_id', '=', 'emp.id')
        // ->select('emp.*', 'cources.cource_name')
        // ->get();


                                
    }
}
