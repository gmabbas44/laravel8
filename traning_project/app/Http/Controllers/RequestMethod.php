<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RequestMethod extends Controller
{
    public function getData(Request $req)
    {
        // return $req->input();
        $data = $req->input('email');
        $req->session()->put('user', $data);
        return redirect('/profile');
    }

    public function profile()
    {
        return view('profile');
    }
}
