<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class cource extends Model
{
    use HasFactory;

    // for relation ship

    public function student()
    {
        return $this->BelongsTo(Student::class, 'std_id');
    }
}
