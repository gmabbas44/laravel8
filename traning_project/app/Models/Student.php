<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    /*  -----------------------------
    *   for one to one relation ship
    *   -----------------------------
    *   Student table is base table 
    *   Cource table is derive table
    */
    public function cource()
    {
        // return $this->hasOne(cource::class, 'std_id');
        return $this->hasOne('App\Models\cource', 'std_id');
    }

    /*  -----------------------------
    *   for one to many relation ship
    *   -----------------------------
    *   Student table is base table 
    *   Cource table is derive table
    */
    public function cources()
    {
    //    return $this->hasMany(cource::class, 'std_id');
       return $this->hasMany('App\Models\cource', 'std_id');
    }

    // many to many relation ship
    public function regs()
    {
        return $this->belongsToMany(Subject::class);

        // for diffrent table or column name
        // return $this->belongsToMany('modelName', 'tableName', 'foreign key-table-1', 'foreign key-table-2');

    }
}
