@extends('layout')

@section('main_content')
<h3>Add New Company Admin</h3>

<div class="card">
    <div class="card-body">
        <form method="POST" action="{{ route('save.company.admin') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="name">Name</label>
                    <input type="text" value="{{ old('name') }}" name="name" class="form-control" id="name" placeholder="Enter name">
                    @error('name')
                    <small id="name" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="username">Username</label>
                    <input type="text" name="username" value="{{ old('username') }}" class="form-control" id="username" placeholder="Enter username">
                    @error('username')
                    <small id="username" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="email">Email</label>
                    <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="email" placeholder="Enter Email">
                    @error('email')
                    <small id="email" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="mobile">Mobile NO</label>
                    <input type="number" name="mobile" value="{{ old('mobile') }}" class="form-control" id="mobile" placeholder="Enter Mobile no">
                    @error('mobile')
                    <small id="mobile" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="status">User Type</label>
                    <select name="user_type_id" id="status"  class="form-control">
                        <option value="">Select one</option>
                        @foreach( $user_types as $t)
                        <option value="{{ $t->id }}">{{ $t->title }}</option>
                        @endforeach
                    </select>
                    @error('user_type_id')
                    <small id="status" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="company">Company</label>
                    <select name="company_id" id="company" class="form-control">
                        <option value="">Select one</option>
                        @foreach( $companies as $c )
                        <option value="{{ $c->id }}">{{ $c->company_name }}</option>
                        @endforeach
                    </select>
                    @error('company_id')
                    <small id="company" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="">Select one</option>
                        <option value="1">Active</option>
                        <option value="0">Deactive</option>
                    </select>
                    @error('status')
                    <small id="status" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Enter password">
                    @error('password')
                    <small id="status" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                    
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>

@endsection()