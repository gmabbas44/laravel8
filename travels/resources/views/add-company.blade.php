@extends('layout')

@section('style')
<style>
.company-img {
    margin: 20px 0;
}
.company-img img {
    width:100px;

}
</style>
@endsection()


@section('main_content')
<h3>Add New Company</h3>

<div class="card">
    <div class="card-body">
    @if( $msg = Session::get('success') )
    <div class="alert alert-primary" role="alert">
        {{ $msg }}
    </div>
    @endif

    @if( count( $errors) > 0)
        <div class="alert alert-danger">
            <ul>
            @foreach( $errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
        <form method="POST" action="{{ route('save.company') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="company-name">Company name</label>
                    <input type="text" name="company_name" class="form-control" id="company-name" placeholder="Company name">
                </div>
                <div class="form-group col-md-6">
                    <label for="email">Email</label>
                    <input type="email" name="company_email" class="form-control" id="email" placeholder="Company email">
                </div>
                <div class="form-group col-md-6">
                    <label for="mobile">Mobile</label>
                    <input type="number" name="mobile" class="form-control" id="mobile" placeholder="Mobile no">
                </div>
                <div class="form-group col-md-6">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="">Select one</option>
                        <option value="1">Active</option>
                        <option value="0">Deactive</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="image">Image</label>
                    <input type="file" name="image" class="form-control" id="image" onchange="proviewImage(this)">
                </div>
            </div>
            <div class="company-img">
                <img id="viewimage" src="{{asset('asset/img/company/avator.png')}}" alt="">
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>

@endsection()

@section('js')
<script>

function proviewImage(input) {
    let image = $('input[type=file]').get(0).files[0];
    if ( image ) {
        let reader = new FileReader();
        reader.onload = function () {
            $('#viewimage').attr({src:reader.result});
        }
        reader.readAsDataURL( image );
    }
}

</script>
@endsection()