@extends('layout')

@section('style')
<style>
    
</style>
@endsection()


@section('main_content')

<h2>Company Admin list</h2>


<div class="table-responsive">
    <table class="table table-sm">
        <thead>
            <tr>
                <th scope="col">SL</th>
                <th scope="col">Name</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col">Mobile</th>
                <th scope="col">User Type</th>
                <th scope="col">Company name</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
        
            @foreach( $users as $u)
            <tr>
                <td>{{ $u->id }}</td>
                <td>{{ $u->name }}</td>
                <td>{{ $u->username }}</td>
                <td>{{ $u->email }}</td>
                <td>{{ $u->mobile }}</td>
                <td>{{ str_replace('_', ' ', $u->title) }} </td>
                <td>{{ $u->company_name }} </td>
                @if( $u->status == 1)
                <td><span class="badge badge-pill badge-primary">Active</span></td>
                @else
                <td><span class="badge badge-pill badge-danger">Deactive</span></td>
                @endif
                <td></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection()



@section('js')

<script>
    $( function () {
        // $('table').DataTable();
    });
</script>
@endsection()