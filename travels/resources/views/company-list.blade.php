@extends('layout')

@section('style')
<style>
    
</style>
@endsection()


@section('main_content')


<h3>Company list</h3>


<div class="table-responsive">
    <table class="table table-sm">
        <thead>
            <tr>
                <th scope="col">SL</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Mobile</th>
                <th scope="col">Image</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach( $companies as $company)
            <tr>
                <td>{{ $company->id }}</td>
                <td>{{ $company->company_name }}</td>
                <td>{{ $company->company_email }}</td>
                <td>{{ $company->mobile }}</td>
                <td>{{ $company->image }}</td>
                @if( $company->status == 1)
                <td><span class="badge badge-pill badge-primary">Active</span></td>
                @else
                <td><span class="badge badge-pill badge-danger">Deactive</span></td>
                @endif
                <td></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection()



@section('js')

<script>
    
</script>
@endsection()