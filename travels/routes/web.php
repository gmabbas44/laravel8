<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\UserController;
use \App\Http\Controllers\AuthorController;
use \App\Http\Controllers\CompanyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/user', [UserController::class, 'index']);
Route::get('/Author', function () {
    return view('login');
});
Route::post('autherlogin', [AuthorController::class, 'login']);
Route::get('author-deshboard', [UserController::class, 'superadmindeshboard']);
Route::get('author/add-company', [CompanyController::class, 'create']);
Route::post('author/save-company', [CompanyController::class, 'store'])->name('save.company');
Route::get('author/company-list', [CompanyController::class, 'index']);
Route::get('author/add-company-admin', [UserController::class, 'create']);
Route::post('author/save-company-admin', [UserController::class, 'store'])->name('save.company.admin');
Route::get('author/company-admin-list', [UserController::class, 'list'])->name('company.admin.list');
