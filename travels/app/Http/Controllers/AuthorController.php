<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\User;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function login( Request $request ) {

        $user = User::where(['email' => $request->email])->first();
        
        if( $user && encryptIt($request->password) === $user->password ) {
            if ($user->user_type_id == 1 || $user->user_type_id == 2 ) {
                $request->session()->put('author', $user);
                return \redirect('author-deshboard');
            } elseif ($user->user_type_id == 3 || $user->user_type_id == 4) {
                $request->session()->put('author', $user);
                // return \redirect('admin-deshboard');
                return \redirect('author-deshboard');
            }
        } else {
            return \redirect('Author');
        }
    }
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Author $author)
    {
        //
    }

    public function edit(Author $author)
    {
        //
    }

    public function update(Request $request, Author $author)
    {
        //
    }

    public function destroy(Author $author)
    {
        //
    }
}
