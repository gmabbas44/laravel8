<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function list()
    {
        //echo encryptIt(1234);
        echo \Session::get('author[user_type_id]');
        if( \Session::get('author[user_type_id]') <= 2  ){
            $users = DB::table('users')
                    ->select('users.id', 'users.name', 'users.username', 'users.email', 'users.mobile', 'users.status', 'user_types.title', 'companies.company_name')
                    ->join('user_types', 'users.user_type_id', '=', 'user_types.id')
                    ->leftJoin('companies', 'users.company_id', '=', 'companies.id')
                    ->get();
        } else {
            $users = DB::table('users')
                    ->select('users.id', 'users.name', 'users.username', 'users.email', 'users.mobile', 'users.status', 'user_types.title', 'companies.company_name')
                    ->join('user_types', 'users.user_type_id', '=', 'user_types.id')
                    ->leftJoin('companies', 'users.company_id', '=', 'companies.id')
                    ->where('.users.company_id', '=', \Session::get('author[company_id]') )
                    ->get();
        }
        
        return view('company-admin-list', ['users' => $users]);
        
    }
    public function superadmindeshboard()
    {
        
        return view('deshboard');
    }

    public function admindeshboard()
    {
        
        return $user['username'];
    }

    public function create()
    {
        $data['user_types'] = DB::table('user_types')->select('id', 'title')->get();
        $data['companies'] = DB::table('companies')->select('id', 'company_name')->get();

        return view('add-company-admin', $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name' => 'required',
            'username' => 'required',
            'email' => 'required',
            'mobile' => 'required',
            'user_type_id' => 'required',
            'company_id' => 'required',
            'status' => 'required',
            'password' => 'required|min:4'
        ]);

        $user = new User;
        $user->name         = $request->name;
        $user->username     = $request->username;
        $user->email        = $request->email;
        $user->mobile       = $request->mobile;
        $user->user_type_id = $request->user_type_id;
        $user->company_id   = $request->company_id;
        $user->status       = $request->status;
        $user->password     = encryptIt($request->password);
        $result = $user->save();

        if ( $result ) {
            return \redirect('company-admin-list');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
