<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function index()
    {
        $companies = Company::all();
        return \view('company-list', ['companies'=>$companies]);
    }

    public function create()
    {
        return view('add-company');
    }

    public function store( Request $request ) 
    {
        // $request->validate([
        //     'file' => 'required|file|mimes:jpg,jpeg,png',
        // ]);
        $company = new Company;
        $image = $request->file('image');
   
        $fileName = time().'.'.$request->file->extension(); ; 
        $request->file->move(public_path('asset/img/company/', $fileName));

        $company->company_name = $request->input('company_name');;
        $company->company_email = $request->input('company_email');;
        $company->mobile = $request->input('mobile');
        $company->status = $request->input('status');
        //$company->image = $request->file('image')->store('image/admin');
        $company->image = $fileName;

        $result = $company->save();
        if ( $result ) {
            return \redirect('author/company-list');
        }
    }

    public function show(Company $company)
    {
        //
    }

    public function edit(Company $company)
    {
        //
    }

    public function update(Request $request, Company $company)
    {
        //
    }

    public function destroy(Company $company)
    {
        //
    }
}
